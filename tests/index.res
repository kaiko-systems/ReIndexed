open RescriptCore
open ReIndexed

let elapsed = time =>
  Promise.make((resolve, _) => {
    let _ = setTimeout(resolve, time)
  })

let uuid4: unit => string = %raw(`
  function () {
    return ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(
      /[018]/g,
      (c) => (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
    );
  }
`)

let thenDo = (promise, fn) => promise->Promise.then(async x => fn(x)->ignore)->ignore
let chooseFrom = choices => choices->Array.getUnsafe(Math.Int.random(0, choices->Array.length))

type trace = Prepared(int) | Run(int)

module MakeIdentifier = () => {
  type t
  external fromString: string => t = "%identity"
  external toString: t => string = "%identity"
  external manyToString: array<t> => array<string> = "%identity"
  external manyFromString: array<string> => array<t> = "%identity"
  let make: unit => t = () => uuid4()->fromString
  let null: t = ""->fromString
  let zero: t = "00000000-0000-0000-0000-00000000000Z"->fromString

  module SortArray = {
    let stableSortInPlace = ids => {
      let ids = ids->manyToString
      ids->Belt.SortArray.String.stableSortInPlace
      ids->manyFromString
    }
  }
}

module VesselIdentifier = MakeIdentifier()

module Vessel = {
  module Def = {
    type t = {id: VesselIdentifier.t, name: string, age: int, flag: option<string>}
    type index = [#id | #name | #age | #flag]
  }
  include MakeIdModel(Def, VesselIdentifier)

  let byAge = (vessel: t) => vessel.age
  let deduplicate: array<t> => array<t> = vessels =>
    Belt.Set.make(~id=module(IdComparator))->Belt.Set.mergeMany(vessels)->Belt.Set.toArray
}

module StaffDef = {
  type position = [#shore | #crew]
  type t = {id: string, name: string, age: int, position: position, tags: array<string>}
  type index = [#id | #name | #age | #position | #tags]
}
module Staff = MakeModel(StaffDef)

module Setup = {
  open IDB.Migration

  type position = [#shore | #crew]
  type staff = Staff.t
  module StaffWriter = ReIndexedPatterns.MakeStaticWriter({
    type t = staff
    let storeName = "staff"

    let data: array<t> = [
      {id: "6595-0c3b", name: "Diego", age: 22, position: #shore, tags: ["A"]},
      {id: "8f4b-76e4", name: "Biego", age: 23, position: #crew, tags: ["A", "B"]},
      {id: "183d-1ad4", name: "Ciego", age: 24, position: #crew, tags: ["B"]},
    ]
  })

  type vessel = Vessel.t
  module VesselWriter = Utils.MakeWriter({
    type t = vessel
    let storeName = "vessels"

    let data: unit => array<t> = _ => [
      /// IMPORTANT: Leave at least one flag with None, so that we can prove
      /// later that the migrations are run in the expected order.
      ///
      /// IMPOTANT: These lines are sorted by key, so that we can test paged
      /// consumers.
      {id: "6c09-dd24"->VesselIdentifier.fromString, name: "MS Anag", age: 15, flag: Some("de")},
      {id: "9454-4ee4"->VesselIdentifier.fromString, name: "MS Anag", age: 10, flag: None},
      {id: "a272-f094"->VesselIdentifier.fromString, name: "Mc Donald", age: 15, flag: Some("de")},
      {id: "c3de-33c4"->VesselIdentifier.fromString, name: "MS Fresco", age: 5, flag: Some("au")},
      {id: "ff06-1ce4"->VesselIdentifier.fromString, name: "Mc Donald", age: 20, flag: None},
    ]
  })

  module VesselRewriter = IDB__Migration__Utils.MakeSimpleInfallibleRewriter({
    type t = vessel
    let storeName = VesselWriter.storeName
    let process = (vessel: t): t => {
      ...vessel,
      flag: vessel.flag->Belt.Option.isNone ? Some("cu") : vessel.flag,
    }
  })
}

module DatabaseDef = {
  open IDB.Migration

  let migrations = () => {
    [
      _ => async (db, _transaction) => {
        let vessels = db->Database.createObjectStore("vessels")
        vessels->Store.createIndex("name", "name")
        vessels->Store.createIndex("age", "age")
        vessels->Store.createIndex("flag", "flag")
        Ok()
      },
      _ => async (_db, transaction) => {
        let res = await Setup.VesselWriter.write(transaction, true)
        Console.debug2("Done writing initial vessel data", res)
        Ok()
      },
      _ => async (_db, transaction) => {
        let res = await Setup.VesselRewriter.rewrite(transaction)
        Console.debug2("Done rewriting the vessels", res)
        Ok()
      },
      _ => async (db, _transaction) => {
        let staff = db->Utils.createStandardStore("staff", ["name", "age", "position"])
        staff->Store.createSimpleMultiEntryIndex("tags")
        Ok()
      },
      _ => async (_db, transaction) => {
        let res = await Setup.StaffWriter.write(
          transaction->IDB__Migration__Transaction.downcast,
          true,
        )
        Console.debug2("Done writing initial staff data", res)
        Ok()
      },
    ]
  }
}

module Database = MakeDatabase(DatabaseDef)

module QueryDef = {
  type read = {vessels: Vessel.read, staff: Staff.read}
  type write = {vessels: Vessel.actions, staff: Staff.actions}
  type response = {vessels: array<Vessel.t>, staff: array<Staff.t>}

  let makeRead = (): read => {vessels: NoOp, staff: NoOp}
  let makeWrite = (): write => {vessels: [], staff: []}
  let makeResponse = () => {vessels: [], staff: []}
}

module Query = Database.MakeQuery(QueryDef)
module Patterns = Database.Patterns

let dbname = "reindexeddb-test-database-v7.0"
IDB.Utils.dropdb(dbname)
->Promise.then(_ => Database.connect(dbname))
->Promise.then(async _db => {
  let vessels: array<Vessel.t> = [
    {id: "a"->VesselIdentifier.fromString, name: "MS Anag", age: 10, flag: None},
    {id: "b"->VesselIdentifier.fromString, name: "MS Anag", age: 15, flag: Some("de")},
    {id: "c"->VesselIdentifier.fromString, name: "MS Fresco", age: 5, flag: Some("au")},
    {id: "d"->VesselIdentifier.fromString, name: "Mc Donald", age: 20, flag: None},
    {id: "x"->VesselIdentifier.fromString, name: "Mc Donald", age: 15, flag: Some("de")},
  ]
  let staff: array<Staff.t> = [
    {id: "a", name: "Diego", age: 22, position: #shore, tags: ["A"]},
    {id: "b", name: "Biego", age: 23, position: #crew, tags: ["A", "B"]},
    {id: "c", name: "Ciego", age: 24, position: #crew, tags: ["B"]},
  ]

  open QUnit

  let setUp = async done => {
    let results = await {
      vessels: [Vessel.Clear]->Array.concat(vessels->Array.map(Vessel.save)),
      staff: [Staff.Clear]->Array.concat(staff->Array.map(Staff.save)),
    }->Query.write
    done()
    results
  }

  module_("Initial state after migrations", _ => {
    asyncTest(
      "Initial data is loaded",
      async a => {
        a->expect(2)

        let {vessels} = await {...Query.makeRead(), vessels: All}->Query.read
        let expected = Setup.VesselWriter.data()->Belt.Array.length
        let found = vessels->Belt.Array.length
        a->equal(
          found,
          expected,
          `There should be ${expected->Int.toString} vessels, found ${found->Int.toString}.`,
        )

        let withFlag = vessels->Belt.Array.keepMap(({flag}) => flag)->Belt.Array.length
        a->equal(withFlag, expected, "All vessels should have a flag")
      },
    )
  })

  module_("Low level bindings and utils (after migrations)", _ => {
    asyncTest(
      "Counting with IDB__Request.count and IDB.Utils.Request",
      async a => {
        a->expect(1)
        let db = Database.connection.db->Option.getUnsafe
        let transaction = db->IDB__Database.transaction(["vessels"], #readonly)

        let count =
          await transaction
          ->IDB__Transaction.objectStore("vessels")
          ->IDB__Store.count
          ->IDB__CountRequest.promise

        let expected = Setup.VesselWriter.data()->Belt.Array.length
        a->equal(count, expected, "Expected to count all vessels")
      },
    )
  })

  module_("Low level patterns (after migrations)", hooks => {
    hooks->beforeEach(
      x => {
        x->timeout(2000)
      },
    )

    asyncTest(
      "MakeCounter",
      async a => {
        module VesselCounter = Patterns.MakeCounter({
          type t = Vessel.t
          let storeName = "vessels"
          let predicate = _ => true
        })

        a->expect(1)

        switch Patterns.transaction(["vessels"], #readonly) {
        | Error(_) => () // Fails with timeout.

        | Ok(transaction) =>
          let count = await VesselCounter.do(transaction)
          a->equal(
            count,
            Setup.VesselWriter.data()->Array.length,
            "There should be exactly the same items after the migrations.",
          )
        }
      },
    )

    asyncTest(
      "MakeGeneralConsumer, read the first item of an index",
      async a => {
        module VesselConsumer = Patterns.MakeGeneralConsumer({
          type t = Vessel.t
          let storeName = "vessels"
          type index = Vessel.index
          type key
          type value
        })

        let request = VesselConsumer.make()->VesselConsumer.limit(1)->VesselConsumer.index(#age)
        let vessels = ref([])
        let collect = v => {
          vessels.contents->Array.push(v)
          true
        }

        a->expect(1)

        switch Patterns.transaction(["vessels"], #readonly) {
        | Error(_) => () // Fails with timeout.
        | Ok(transaction) =>
          let _ = await request->VesselConsumer.do(transaction, Value(collect))

          a->deepEqual(
            vessels.contents,
            [
              {
                id: "c3de-33c4"->VesselIdentifier.fromString,
                name: "MS Fresco",
                age: 5,
                flag: Some("au"),
              },
            ],
            "The youngest vessel",
          )
        }
      },
    )

    asyncTest(
      "MakeGeneralConsumer, read the last item of an index",
      async a => {
        module VesselConsumer = Patterns.MakeGeneralConsumer({
          type t = Vessel.t
          let storeName = "vessels"
          type index = Vessel.index
          type key
          type value
        })

        let request =
          VesselConsumer.make()
          ->VesselConsumer.limit(1)
          ->VesselConsumer.index(#age)
          ->VesselConsumer.direction(#prev)

        let vessels = ref([])
        let collect = v => {
          vessels.contents->Array.push(v)
          true
        }
        a->expect(1)

        switch Patterns.transaction(["vessels"], #readonly) {
        | Error(_) => () // Fails with timeout.
        | Ok(transaction) =>
          let _ = await request->VesselConsumer.do(transaction, Value(collect))

          a->deepEqual(
            vessels.contents,
            [
              {
                id: "ff06-1ce4"->VesselIdentifier.fromString,
                name: "Mc Donald",
                age: 20,
                flag: Some("cu"),
              },
            ],
            "The oldest vessel",
          )
        }
      },
    )

    asyncTest(
      "MakePagedConsumer with offset and limit 1",
      async a => {
        let counter = ref(0)
        module VesselConsumer = Patterns.MakePagedConsumer({
          type t = Vessel.t
          let storeName = "vessels"
          let offset = Setup.VesselWriter.data()->Array.length - 2
          let limit = Some(1)
          let process = _ => {
            counter := counter.contents + 1
            true
          }
        })
        a->expect(1)

        switch Patterns.transaction(["vessels"], #readonly) {
        | Error(_) => () // Fails with timeout.
        | Ok(transaction) =>
          let _ = await VesselConsumer.read(transaction)
          a->equal(counter.contents, 1, "We asked for 1, we should get 1")
        }
      },
    )

    asyncTest(
      "MakePagedConsumer with offset beyond bounds and no limit",
      async a => {
        let counter = ref(0)
        module VesselConsumer = Patterns.MakePagedConsumer({
          type t = Vessel.t
          let storeName = "vessels"
          let offset = Setup.VesselWriter.data()->Array.length
          let limit = None
          let process = _ => {
            counter := counter.contents + 1
            true
          }
        })
        a->expect(1)
        switch Patterns.transaction(["vessels"], #readonly) {
        | Error(_) => () // Fails with timeout.
        | Ok(transaction) =>
          let _ = await VesselConsumer.read(transaction)
          a->equal(counter.contents, 0, "We asked beyound count, we get nothing")
        }
      },
    )

    asyncTest(
      "MakePagedReader with offset (-2) and no limit",
      async a => {
        module VesselConsumer = Patterns.MakePagedReader({
          type t = Vessel.t
          let storeName = "vessels"
          let offset = Setup.VesselWriter.data()->Array.length - 2
          let limit = None
        })
        a->expect(1)
        switch Patterns.transaction(["vessels"], #readonly) {
        | Error(_) => () // Fails with timeout.
        | Ok(transaction) =>
          let items = await VesselConsumer.read(transaction)
          a->deepEqual(
            items,
            [
              {
                id: "c3de-33c4"->VesselIdentifier.fromString,
                name: "MS Fresco",
                age: 5,
                flag: Some("au"),
              },
              {
                id: "ff06-1ce4"->VesselIdentifier.fromString,
                name: "Mc Donald",
                age: 20,
                flag: Some("cu"),
              },
            ],
            "The last two items of the migration (with CU inplace of None in the last item)",
          )
        }
      },
    )

    asyncTest(
      "MakePagedReader with offset beyond limits",
      async a => {
        module VesselConsumer = Patterns.MakePagedReader({
          type t = Vessel.t
          let storeName = "vessels"
          let offset = Setup.VesselWriter.data()->Array.length
          let limit = None
        })
        a->expect(1)

        switch Patterns.transaction(["vessels"], #readonly) {
        | Error(_) => () // Fails with timeout.
        | Ok(transaction) =>
          let items = await VesselConsumer.read(transaction)
          a->deepEqual(items, [], "Offset beyond the end")
        }
      },
    )

    asyncTest(
      "MakeReader",
      async a => {
        module VesselConsumer = Patterns.MakeReader({
          type t = Vessel.t
          let storeName = "vessels"
        })
        a->expect(1)
        switch Patterns.transaction(["vessels"], #readonly) {
        | Error(_) => () // Fails with timeout.
        | Ok(transaction) =>
          let items = await VesselConsumer.read(transaction)
          a->deepEqual(
            items,
            Setup.VesselWriter.data()->Array.map(Setup.VesselRewriter.process),
            "The data in the migration",
          )
        }
      },
    )
  })

  module_("Base", hooks => {
    hooks->beforeEach(
      x => {
        x->timeout(10_000)
        let done = x->async
        switch setUp(done) {
        | x => x->ignore
        | exception any => Console.error2(__MODULE__, any)
        }
      },
    )

    asyncTest(
      "Can read all the stored vessels",
      async x => {
        let results = await {...Query.makeRead(), vessels: All}->Query.read
        x->equal(results.vessels->Array.length, 5, "There should be 5 vessels")
        x->deepEqual(results.vessels, vessels, "The vessels match the stored vessels")
      },
    )

    asyncTest(
      "Can delete all stored vessels at once",
      async x => {
        let results = await {...Query.makeRead(), vessels: All}->Query.read
        x->equal(results.vessels->Array.length, 5, "There should be 5 vessels")

        let results =
          await [
            Write(_ => {...Query.makeWrite(), vessels: [Clear]}),
            Read(_ => {...Query.makeRead(), vessels: All}),
          ]->Query.do
        x->equal(results.vessels->Array.length, 0, "There most be no vessels stored after 'clear'")
      },
    )

    asyncTest(
      "Can delete a single vessel by key",
      async x => {
        let results =
          await [
            Write(_ => {...Query.makeWrite(), vessels: [Delete("a"->VesselIdentifier.fromString)]}),
            Read(_ => {...Query.makeRead(), vessels: All}),
          ]->Query.do

        x->equal(results.vessels->Array.length, 4, "There should be just 4 vessels")
        x->isFalse(
          results.vessels->Array.some(vessel => vessel.id == "a"->VesselIdentifier.fromString),
          "Non of the vessel has 'a' as id",
        )
      },
    )

    asyncTest(
      "Can retrieve a single vessel by key",
      async x => {
        let results =
          await {...Query.makeRead(), vessels: Get("a"->VesselIdentifier.fromString)}->Query.read
        x->equal(results.vessels->Array.length, 1, "There should be just 1 vessels")
        x->deepEqual(
          results.vessels->Array.get(0),
          Some({id: "a"->VesselIdentifier.fromString, name: "MS Anag", age: 10, flag: None}),
          "The vessel retrieved should have id 'a'",
        )
      },
    )

    asyncTest(
      "Can add a new vessel",
      async x => {
        let results = await [
          Write(
            _ => {
              ...Query.makeWrite(),
              vessels: [
                Save({
                  id: "new"->VesselIdentifier.fromString,
                  name: "MS New",
                  age: 30,
                  flag: None,
                }),
              ],
            },
          ),
          Read(_ => {...Query.makeRead(), vessels: All}),
        ]->Query.do
        x->equal(results.vessels->Array.length, 6, "There should be just 6 vessels")
        x->deepEqual(
          results.vessels->Array.filter(vessel => vessel.id == "new"->VesselIdentifier.fromString),
          [{id: "new"->VesselIdentifier.fromString, name: "MS New", age: 30, flag: None}],
          "The new vessel can be retrieved",
        )
      },
    )

    let checkResultantKeysInVessels = async (x, query, keys) => {
      let results = await {...Query.makeRead(), vessels: query}->Query.read
      let retrievedKeys = results.vessels->Array.map(vessel => vessel.id)
      x->deepEqual(
        retrievedKeys,
        keys->VesselIdentifier.manyFromString,
        "Retrieved should keys match",
      )
    }

    let checkResultantKeysInStaff = async (x, query, keys) => {
      let results = await {...Query.makeRead(), staff: query}->Query.read
      let retrievedKeys = results.staff->Array.map(staff => staff.id)
      x->deepEqual(retrievedKeys, keys, "Retrieved should keys match")
    }

    module_(
      "Limit: All",
      _ => {
        asyncTest(
          "Limit 0",
          x => {
            x->checkResultantKeysInVessels(Limit(0, All), [])
          },
        )

        asyncTest(
          "Limit 2",
          x => {
            x->checkResultantKeysInVessels(Limit(2, All), ["a", "b"])
          },
        )

        asyncTest(
          "Limit 200",
          x => {
            x->checkResultantKeysInVessels(Limit(200, All), ["a", "b", "c", "d", "x"])
          },
        )
      },
    )

    module_(
      "Offset: All",
      _ => {
        asyncTest(
          "Offset 200",
          x => {
            x->checkResultantKeysInVessels(Offset(200, All), [])
          },
        )

        asyncTest(
          "Offset 2",
          x => {
            x->checkResultantKeysInVessels(Offset(2, All), ["c", "d", "x"])
          },
        )

        asyncTest(
          "Offset 0",
          x => {
            x->checkResultantKeysInVessels(Offset(0, All), ["a", "b", "c", "d", "x"])
          },
        )
      },
    )

    module_(
      "Primary key",
      _ => {
        asyncTest(
          "Is(#id)",
          x => {
            x->checkResultantKeysInVessels(Is(#id, "c"), ["c"])
          },
        )

        asyncTest(
          "Limit(0, Is(#id))",
          x => {
            x->checkResultantKeysInVessels(Limit(0, Is(#id, "c")), [])
          },
        )

        asyncTest(
          "Limit(2, Is(#id))",
          x => {
            x->checkResultantKeysInVessels(Limit(2, Is(#id, "c")), ["c"])
          },
        )

        asyncTest(
          "Offset(1, Is(#id))",
          x => {
            x->checkResultantKeysInVessels(Offset(1, Is(#id, "c")), [])
          },
        )

        asyncTest(
          "In: Can get many vessels with In",
          x => {
            x->checkResultantKeysInVessels(
              In(["a", "c", "this-doesnt-exists"]->VesselIdentifier.manyFromString),
              ["a", "c"],
            )
          },
        )

        asyncTest(
          "Limit(0, In(...))",
          x => {
            x->checkResultantKeysInVessels(
              Limit(0, In(["a", "c", "this-doesnt-exists"]->VesselIdentifier.manyFromString)),
              [],
            )
          },
        )

        asyncTest(
          "Limit(1, In(...))",
          x => {
            x->checkResultantKeysInVessels(
              Limit(1, In(["a", "c", "this-doesnt-exists"]->VesselIdentifier.manyFromString)),
              ["a"],
            )
          },
        )

        asyncTest(
          "Limit(10, In(...))",
          x => {
            x->checkResultantKeysInVessels(
              Limit(10, In(["a", "c", "this-doesnt-exists"]->VesselIdentifier.manyFromString)),
              ["a", "c"],
            )
          },
        )

        asyncTest(
          "Offset(0, In(...))",
          x => {
            x->checkResultantKeysInVessels(
              Offset(0, In(["a", "c", "this-doesnt-exists"]->VesselIdentifier.manyFromString)),
              ["a", "c"],
            )
          },
        )

        asyncTest(
          "Offset(1, In(...))",
          x => {
            x->checkResultantKeysInVessels(
              Offset(1, In(["a", "c", "this-doesnt-exists"]->VesselIdentifier.manyFromString)),
              ["c"],
            )
          },
        )

        asyncTest(
          "Offset(10, In(...))",
          x => {
            x->checkResultantKeysInVessels(
              Offset(10, In(["a", "c", "this-doesnt-exists"]->VesselIdentifier.manyFromString)),
              [],
            )
          },
        )

        asyncTest(
          "Can get multiple staff by using tag A",
          x => {
            x->checkResultantKeysInStaff(Is(#tags, "A"), ["a", "b"])
          },
        )

        asyncTest(
          "Limit 0: Can get multiple staff by using tag A",
          x => {
            x->checkResultantKeysInStaff(Limit(0, Is(#tags, "A")), [])
          },
        )

        asyncTest(
          "Limit 1: Can get multiple staff by using tag A",
          x => {
            x->checkResultantKeysInStaff(Limit(1, Is(#tags, "A")), ["a"])
          },
        )

        asyncTest(
          "Offset 20: Can get multiple staff by using tag A",
          x => {
            x->checkResultantKeysInStaff(Offset(20, Is(#tags, "A")), [])
          },
        )

        asyncTest(
          "Can get multiple staff by using tag B",
          x => {
            x->checkResultantKeysInStaff(Is(#tags, "B"), ["b", "c"])
          },
        )

        asyncTest(
          "Gets no staff by using tag C",
          x => {
            x->checkResultantKeysInStaff(Is(#tags, "C"), [])
          },
        )

        asyncTest(
          "Can get less or equal than primary key",
          x => {
            x->checkResultantKeysInVessels(Lte(#id, "c"), ["a", "b", "c"])
          },
        )

        asyncTest(
          "Limit 1: Can get less or equal than primary key",
          x => {
            x->checkResultantKeysInVessels(Limit(1, Lte(#id, "c")), ["a"])
          },
        )

        asyncTest(
          "Offset 2: Can get less or equal than primary key",
          x => {
            x->checkResultantKeysInVessels(Offset(2, Lte(#id, "c")), ["c"])
          },
        )

        asyncTest(
          "Can get greater than primary key",
          x => {
            x->checkResultantKeysInVessels(Gt(#id, "c"), ["d", "x"])
          },
        )

        asyncTest(
          "Can get greater or equal than primary key",
          x => {
            x->checkResultantKeysInVessels(Gte(#id, "c"), ["c", "d", "x"])
          },
        )

        asyncTest(
          "Can get a range with exclusive keys",
          x => {
            x->checkResultantKeysInVessels(Between(#id, Excl("b"), Excl("d")), ["c"])
          },
        )

        asyncTest(
          "Can get a range left exclusive and right inclusive keys",
          x => {
            x->checkResultantKeysInVessels(Between(#id, Excl("b"), Incl("d")), ["c", "d"])
          },
        )

        asyncTest(
          "Limit 1: Can get a range left exclusive and right inclusive keys",
          x => {
            x->checkResultantKeysInVessels(Limit(1, Between(#id, Excl("b"), Incl("d"))), ["c"])
          },
        )

        asyncTest(
          "Can get a range left inclusive and right exclusive keys",
          x => {
            x->checkResultantKeysInVessels(Between(#id, Incl("b"), Excl("d")), ["b", "c"])
          },
        )

        asyncTest(
          "Offset 1: Can get a range left inclusive and right exclusive keys",
          x => {
            x->checkResultantKeysInVessels(Offset(1, Between(#id, Incl("b"), Excl("d"))), ["c"])
          },
        )

        asyncTest(
          "Can get a range of incluisive keys",
          x => {
            x->checkResultantKeysInVessels(Between(#id, Incl("b"), Incl("d")), ["b", "c", "d"])
          },
        )

        asyncTest(
          "StartsWith(#name, MS)",
          x => x->checkResultantKeysInVessels(StartsWith(#name, "MS"), ["a", "b", "c"]),
        )

        asyncTest(
          "Limit(1, StartsWith(#name, MS))",
          x => x->checkResultantKeysInVessels(Limit(1, StartsWith(#name, "MS")), ["a"]),
        )

        asyncTest(
          "Offset 2: StartsWith(#name, MS)",
          x => x->checkResultantKeysInVessels(Offset(2, StartsWith(#name, "MS")), ["c"]),
        )

        asyncTest(
          "StartsWith(#name, Mc)",
          x => x->checkResultantKeysInVessels(StartsWith(#name, "Mc"), ["d", "x"]),
        )

        asyncTest(
          "StartsWith(#name, ms)",
          x => x->checkResultantKeysInVessels(StartsWith(#name, "ms"), []),
        )

        asyncTest("Max(#age)", x => x->checkResultantKeysInStaff(Max(#age), ["c"]))
        asyncTest("Min(#age)", x => x->checkResultantKeysInStaff(Min(#age), ["a"]))

        asyncTest("Max(#name)", x => x->checkResultantKeysInStaff(Max(#name), ["a"]))
        asyncTest("Min(#name)", x => x->checkResultantKeysInStaff(Min(#name), ["b"]))
      },
    )

    module_(
      "Test term on an index",
      _ => {
        asyncTest(
          "Can read non null flags",
          x => {
            x->checkResultantKeysInVessels(NotNull(#flag), ["c", "b", "x"])
          },
        )

        asyncTest(
          "Limit: Can read non null flags",
          x => {
            x->checkResultantKeysInVessels(Limit(1, NotNull(#flag)), ["c"])
          },
        )

        asyncTest(
          "Can get pricese primary key",
          x => {
            x->checkResultantKeysInVessels(Is(#age, Query.value(15)), ["b", "x"])
          },
        )

        asyncTest(
          "Can get less than primary key",
          x => {
            x->checkResultantKeysInVessels(Lt(#age, Query.value(15)), ["c", "a"])
          },
        )

        asyncTest(
          "Can get less or equal than primary key",
          x => {
            x->checkResultantKeysInVessels(Lte(#age, Query.value(15)), ["c", "a", "b", "x"])
          },
        )

        asyncTest(
          "Can get greater than primary key",
          x => {
            x->checkResultantKeysInVessels(Gt(#age, Query.value(15)), ["d"])
          },
        )

        asyncTest(
          "Can get greater or equal than primary key",
          x => {
            x->checkResultantKeysInVessels(Gte(#age, Query.value(15)), ["b", "x", "d"])
          },
        )

        asyncTest(
          "Can get a range with exclusive of keys",
          x => {
            x->checkResultantKeysInVessels(
              Between(#age, Excl(Query.value(5)), Excl(Query.value(20))),
              ["a", "b", "x"],
            )
          },
        )

        asyncTest(
          "Can get a range left exclusive and right inclusive keys",
          x => {
            x->checkResultantKeysInVessels(
              Between(#age, Excl(Query.value(5)), Incl(Query.value(20))),
              ["a", "b", "x", "d"],
            )
          },
        )

        asyncTest(
          "Can get a range left inclusive and right exclusive keys",
          x => {
            x->checkResultantKeysInVessels(
              Between(#age, Incl(Query.value(5)), Excl(Query.value(20))),
              ["c", "a", "b", "x"],
            )
          },
        )

        asyncTest(
          "Can get a range of incluisive keys",
          x => {
            x->checkResultantKeysInVessels(
              Between(#age, Incl(Query.value(5)), Incl(Query.value(20))),
              ["c", "a", "b", "x", "d"],
            )
          },
        )
      },
    )

    module_(
      "AND logic operator",
      _ => {
        asyncTest(
          "Two queries over the same value result in empty",
          x => {
            x->checkResultantKeysInVessels(And(Is(#name, "MS Anag"), Is(#name, "MS Donald")), [])
          },
        )

        asyncTest(
          "Two where one reduces the other one",
          x => {
            x->checkResultantKeysInVessels(
              And(Is(#name, "MS Anag"), Is(#age, Query.value(15))),
              ["b"],
            )
          },
        )

        asyncTest(
          "Two where one reduces the other one (Lte)",
          x => {
            x->checkResultantKeysInVessels(
              And(Is(#name, "MS Anag"), Lte(#age, Query.value(15))),
              ["a", "b"],
            )
          },
        )

        asyncTest(
          "Limit 1: Two where one reduces the other one (Lte)",
          x => {
            x->checkResultantKeysInVessels(
              Limit(1, And(Is(#name, "MS Anag"), Lte(#age, Query.value(15)))),
              ["a"],
            )
          },
        )

        asyncTest(
          "Limit 1, Limit 2: Two where one reduces the other one (Lte)",
          x => {
            x->checkResultantKeysInVessels(
              Limit(1, Limit(2, And(Is(#name, "MS Anag"), Lte(#age, Query.value(15))))),
              ["a"],
            )
          },
        )

        asyncTest(
          "Offset 1: Two where one reduces the other one (Lte)",
          x => {
            x->checkResultantKeysInVessels(
              Offset(1, And(Is(#name, "MS Anag"), Lte(#age, Query.value(15)))),
              ["b"],
            )
          },
        )

        asyncTest(
          "Offset 10: Two where one reduces the other one (Lte)",
          x => {
            x->checkResultantKeysInVessels(
              Offset(10, And(Is(#name, "MS Anag"), Lte(#age, Query.value(15)))),
              [],
            )
          },
        )
      },
    )

    module_(
      "OR logic operator",
      _ => {
        asyncTest(
          "Two queries on oposite directions exclude the center",
          x => {
            x->checkResultantKeysInVessels(
              Or(Lt(#age, Query.value(15)), Gt(#age, Query.value(15))),
              ["c", "a", "d"],
            )
          },
        )

        asyncTest(
          "Limit 1: Two queries on oposite directions exclude the center",
          x => {
            x->checkResultantKeysInVessels(
              Limit(1, Or(Lt(#age, Query.value(15)), Gt(#age, Query.value(15)))),
              ["c"],
            )
          },
        )

        asyncTest(
          "Offset 2: Two queries on oposite directions exclude the center",
          x => {
            x->checkResultantKeysInVessels(
              Offset(2, Or(Lt(#age, Query.value(15)), Gt(#age, Query.value(15)))),
              ["d"],
            )
          },
        )

        asyncTest(
          "Or(Limit(1, ...) Limit(1, ...)): Or behaves like UNION.",
          x => {
            x->checkResultantKeysInVessels(
              Or(
                Or(Limit(1, Is(#age, Query.value(5))), Limit(1, Gte(#age, Query.value(15)))),
                Limit(1, Gte(#age, Query.value(16))),
              ),
              ["c", "b", "d"],
            )
          },
        )
      },
    )

    module_(
      "AnyOf operator",
      _ => {
        asyncTest(
          "Quering a single item that is not present returns nothing",
          x => {
            x->checkResultantKeysInVessels(AnyOf(#age, [Query.value(17)]), [])
          },
        )

        asyncTest(
          "Quering a on a single value can return multiple candidates",
          x => {
            x->checkResultantKeysInVessels(AnyOf(#name, ["MS Anag"]), ["a", "b"])
          },
        )

        asyncTest(
          "Quering a on a multiple values returns multiple candidates",
          x => {
            x->checkResultantKeysInVessels(
              AnyOf(#name, ["MS Anag", "Mc Donald", "MS Anag"]),
              ["a", "b", "d", "x"],
            )
          },
        )

        asyncTest(
          "Offset 2, Limit 2: Quering a on a multiple values returns multiple candidates",
          x => {
            x->checkResultantKeysInVessels(
              Offset(2, Limit(2, AnyOf(#name, ["MS Anag", "Mc Donald", "MS Anag"]))),
              ["d", "x"],
            )
          },
        )

        asyncTest(
          "Limit 2, Offset 2: Quering a on a multiple values returns multiple candidates",
          x => {
            x->checkResultantKeysInVessels(
              Limit(2, Offset(2, AnyOf(#name, ["MS Anag", "Mc Donald", "MS Anag"]))),
              ["d", "x"],
            )
          },
        )

        asyncTest(
          "Quering with in can be combined with logical operators",
          x => {
            x->checkResultantKeysInVessels(
              Or(AnyOf(#name, ["MS Anag", "Mc Donald"]), AnyOf(#age, [Query.value(5)])),
              ["c", "a", "b", "d", "x"],
            )
          },
        )

        asyncTest(
          "Offset 3: Quering with in can be combined with logical operators",
          x => {
            x->checkResultantKeysInVessels(
              Offset(3, Or(AnyOf(#name, ["MS Anag", "Mc Donald"]), AnyOf(#age, [Query.value(5)]))),
              ["d", "x"],
            )
          },
        )
      },
    )

    module_(
      "NoneOf operator",
      _ => {
        asyncTest(
          "Quering a single item that is not present returns everything",
          x => {
            x->checkResultantKeysInVessels(
              NoneOf(#age, [Query.value(17)]),
              ["c", "a", "b", "x", "d"],
            )
          },
        )

        asyncTest(
          "Offset 2, Limit 1: Quering a single item that is not present returns everything",
          x => {
            x->checkResultantKeysInVessels(
              Limit(1, Offset(2, NoneOf(#age, [Query.value(17)]))),
              ["b"],
            )
          },
        )

        asyncTest(
          "Rejecting single value yield may results",
          x => {
            x->checkResultantKeysInVessels(NoneOf(#name, ["MS Anag"]), ["c", "d", "x"])
          },
        )

        asyncTest(
          "Reject multiple values yield less results",
          x => {
            x->checkResultantKeysInVessels(
              NoneOf(#name, ["MS Anag", "Mc Donald", "MS Anag"]),
              ["c"],
            )
          },
        )

        asyncTest(
          "Quering with in can be combined with logical operators",
          x => {
            x->checkResultantKeysInVessels(
              Or(NoneOf(#name, ["MS Anag", "Mc Donald"]), AnyOf(#age, [Query.value(5)])),
              ["c"],
            )
          },
        )
      },
    )

    let checkRemainingItems = async (query, x, expected) => {
      let _ = await query
      let results = await {...Query.makeRead(), vessels: All}->Query.read
      x->deepEqual(results.vessels, expected, "Should match the remaining objects")
    }

    module_(
      "Conditional update",
      _ => {
        asyncTest(
          "If the predicate returns None nothing is updated",
          x => {
            [
              Read(_ => {...Query.makeRead(), vessels: All}),
              Write(
                ({vessels}) => {
                  ...Query.makeWrite(),
                  vessels: vessels->Array.filterMap(
                    vessel =>
                      if vessel.age == 100 {
                        Some(Vessel.Save({...vessel, age: 0}))
                      } else {
                        None
                      },
                  ),
                },
              ),
            ]
            ->Query.do
            ->checkRemainingItems(
              x,
              [
                {id: "a"->VesselIdentifier.fromString, name: "MS Anag", age: 10, flag: None},
                {id: "b"->VesselIdentifier.fromString, name: "MS Anag", age: 15, flag: Some("de")},
                {
                  id: "c"->VesselIdentifier.fromString,
                  name: "MS Fresco",
                  age: 5,
                  flag: Some("au"),
                },
                {id: "d"->VesselIdentifier.fromString, name: "Mc Donald", age: 20, flag: None},
                {
                  id: "x"->VesselIdentifier.fromString,
                  name: "Mc Donald",
                  age: 15,
                  flag: Some("de"),
                },
              ],
            )
          },
        )

        asyncTest(
          "If the the predicate returns Some(vessel) it is modified",
          x => {
            [
              Read(_ => {...Query.makeRead(), vessels: Is(#name, "MS Anag")}),
              Write(
                ({vessels}) => {
                  ...Query.makeWrite(),
                  vessels: vessels->Array.map(vessel => Vessel.Save({...vessel, age: 0})),
                },
              ),
            ]
            ->Query.do
            ->checkRemainingItems(
              x,
              [
                {id: "a"->VesselIdentifier.fromString, name: "MS Anag", age: 0, flag: None},
                {id: "b"->VesselIdentifier.fromString, name: "MS Anag", age: 0, flag: Some("de")},
                {
                  id: "c"->VesselIdentifier.fromString,
                  name: "MS Fresco",
                  age: 5,
                  flag: Some("au"),
                },
                {id: "d"->VesselIdentifier.fromString, name: "Mc Donald", age: 20, flag: None},
                {
                  id: "x"->VesselIdentifier.fromString,
                  name: "Mc Donald",
                  age: 15,
                  flag: Some("de"),
                },
              ],
            )
          },
        )

        asyncTest(
          "Deleting the ones with `age = 15` should left the rest intact",
          x => {
            [
              Read(_ => {...Query.makeRead(), vessels: Is(#age, Query.value(15))}),
              Write(
                ({vessels}) => {
                  ...Query.makeWrite(),
                  vessels: vessels->Array.map(Vessel.remove),
                },
              ),
            ]
            ->Query.do
            ->checkRemainingItems(
              x,
              [
                {id: "a"->VesselIdentifier.fromString, name: "MS Anag", age: 10, flag: None},
                {
                  id: "c"->VesselIdentifier.fromString,
                  name: "MS Fresco",
                  age: 5,
                  flag: Some("au"),
                },
                {id: "d"->VesselIdentifier.fromString, name: "Mc Donald", age: 20, flag: None},
              ],
            )
          },
        )
      },
    )
  })

  module_("Integrity/consistency tests", hooks => {
    hooks->beforeEach(
      x => {
        x->timeout(10_000)
      },
    )

    asyncTest(
      "'.write' should be ran predictably",
      async a => {
        a->expect(1)
        let id = "356fe8db-947c-4706-b0c8-9732d682c824"->VesselIdentifier.fromString
        let clones =
          Array.make(~length=10_000, 0)->Array.flatMap(
            (_): array<Vessel.t> => [
              {id: "a"->VesselIdentifier.fromString, name: "Vessel", age: 10, flag: None},
              {id: "b"->VesselIdentifier.fromString, name: "Vessel", age: 10, flag: None},
              {id: "c"->VesselIdentifier.fromString, name: "Vessel", age: 10, flag: None},
              {id: "d"->VesselIdentifier.fromString, name: "Vessel", age: 10, flag: None},
              {id, name: "Vessel", age: 10, flag: None},
            ],
          )
        let actions = clones->Array.map(Vessel.save)
        let actions = actions->Array.concat([Vessel.Delete(id)])
        let _ = await {...Query.makeWrite(), vessels: actions}->Query.write
        let {vessels} = await {...Query.makeRead(), vessels: Vessel.Get(id)}->Query.read
        a->equal(vessels->Array.length, 0, "There should be no vessel with the id")
      },
    )
  })

  module_("Performance test", _ => {
    let names = ["MS Angst", "MS Angust", "Cachimba", "Record MSX", "VAX", "UNIVAC"]
    let flags = [None, Some("de"), Some("cu"), Some("ch"), Some("au"), Some("tk")]

    asyncTest(
      "Bulk insert of 10k items",
      async x => {
        let vessels = Array.make(~length=10_000, 0)->Array.map(
          (_): Vessel.t => {
            id: VesselIdentifier.make(),
            name: chooseFrom(names),
            age: Math.Int.random(0, 100),
            flag: chooseFrom(flags),
          },
        )
        let start = Date.now()->Int.fromFloat
        let _ = await {...Query.makeWrite(), vessels: vessels->Array.map(Vessel.save)}->Query.write
        let afterInsert = Date.now()->Int.fromFloat
        x->isTrue(true, `Insertion done in ${(afterInsert - start)->Int.toString}ms`)

        let _ = await {...Query.makeRead(), vessels: All}->Query.read
        let afterReadAll = Date.now()->Int.fromFloat
        x->isTrue(true, `Read all done in ${(afterReadAll - afterInsert)->Int.toString}ms`)

        let _ = await {...Query.makeRead(), vessels: Is(#name, "Cachimba")}->Query.read
        let readFromIndex = Date.now()->Int.fromFloat
        x->isTrue(
          true,
          `Read from single index in ${(readFromIndex - afterReadAll)->Int.toString}ms`,
        )
      },
    )

    asyncTest(
      "Bulk insert of 10k using chaining",
      async x => {
        let vessels = Array.make(~length=10_000, 0)->Array.map(
          (_): Vessel.t => {
            id: VesselIdentifier.make(),
            name: chooseFrom(names),
            age: Math.Int.random(0, 100),
            flag: chooseFrom(flags),
          },
        )

        let _ =
          await [
            Write(_ => {...Query.makeWrite(), vessels: vessels->Array.map(Vessel.save)}),
            Read(_ => {...Query.makeRead(), vessels: Limit(10, All)}),
            Read(_ => {...Query.makeRead(), vessels: Limit(10, Is(#name, "Cachimba"))}),
          ]->Query.do

        x->isTrue(true, "Finished")
      },
    )
  })

  module_("Migrations", _ => {
    module Database = MakeDatabase({
      let migrations = () => [_ => async (_db, _transaction) => Ok()]
    })

    asyncTest(
      "Migrations phase two runs",
      async a => {
        let trace: array<trace> = []
        let firstIndex = Database.migrations()->Array.length

        module NextOkDatabase = MakeDatabase({
          let migrations = () =>
            Database.migrations()->Array.concat([
              index => {
                Console.debug2("Preparing migration", index)
                trace->Array.push(Prepared(index))

                async (_db, _transaction) => {
                  trace->Array.push(Run(index))
                  Ok()
                }
              },
              index => {
                Console.debug2("Preparing migration", index)
                trace->Array.push(Prepared(index))

                async (_db, _transaction) => {
                  trace->Array.push(Run(index))
                  Ok()
                }
              },
            ])
        })

        a->expect(1)
        let dbname = `reindexeddb-test-database-lowlevel-${uuid4()}`

        let _ = await Database.connect(dbname)
        Database.disconnect()

        let _ = await NextOkDatabase.connect(dbname)
        a->deepEqual(
          trace,
          [Prepared(firstIndex), Prepared(firstIndex + 1), Run(firstIndex), Run(firstIndex + 1)],
          "Migrations run in two phases",
        )

        Console.debug2("Deleting DB", dbname)
        let _ = await NextOkDatabase.drop()
      },
    )

    asyncTest(
      "Failing migrations leave the DB unchanged and stop",
      async a => {
        let trace: array<trace> = []
        let firstIndex = Database.migrations()->Array.length

        module FailingDatabase = MakeDatabase({
          let migrations = () =>
            Database.migrations()->Array.concat([
              index => {
                Console.debug2("Preparing migration", index)
                trace->Array.push(Prepared(index))
                async (_db, _transaction) => {
                  trace->Array.push(Run(index))
                  Error("Failed")
                }
              },
              index => {
                Console.debug2("Preparing migration", index)
                trace->Array.push(Prepared(index))
                async (_db, _transaction) => {
                  trace->Array.push(Run(index))
                  Ok()
                }
              },
            ])
        })

        a->expect(2)
        a->timeout(10_000)

        let dbname = "reindexeddb-test-database-failing-v7.0"

        /// Install the first valid migrations
        let _ = await Database.connect(dbname)
        Database.disconnect()

        /// Now, try to open the DB with failing migrations.
        let res = await FailingDatabase.connect(dbname)
        a->deepEqual(
          trace,
          [Prepared(firstIndex), Prepared(firstIndex + 1), Run(firstIndex)],
          "All migrations prepared but only the first runs",
        )
        a->isTrue(res->Belt.Result.isError, "DB in migrations")
        IDB.Utils.dropdb(dbname)->ignore
      },
    )
  })

  QUnit.start()
  QUnit.done(async () => {
    await elapsed(6000)
    Database.disconnect()
    let _ = await IDB.Utils.dropdb(dbname)
  })
})
->ignore
