@@uncurried

open IDB
open RescriptCore

type index = string
type value = string
type bound =
  | Incl(value)
  | Excl(value)

type rec read<'a, 'key> =
  | NoOp
  | Get(value)
  // index queries
  | All
  | In(array<value>)
  | NotIn(array<value>)
  | Is(index, value)
  | NotNull(index)
  | Lt(index, value)
  | Lte(index, value)
  | Gt(index, value)
  | Gte(index, value)
  | Between(index, bound, bound)
  | AnyOf(index, array<value>)
  | NoneOf(index, array<value>)
  | StartsWith(index, string)
  | Max(index)
  | Min(index)
  // recursive queries
  | And(read<'a, 'key>, read<'a, 'key>)
  | Or(read<'a, 'key>, read<'a, 'key>)
  // Limit & offset
  | Limit(int, read<'a, 'key>)
  | Offset(int, read<'a, 'key>)

type write<'a> =
  | Clear
  | Save('a)
  | Delete(value)

type response<'a> = Dict.t<array<'a>>
type query<'a, 'key> =
  | Read(response<'a> => Dict.t<read<'a, 'key>>)
  | Write(response<'a> => Dict.t<array<write<'a>>>)

let _getAll = (store, attribute, range, callback, ~limit: _=?) => {
  /// XXX: IDB's getAll(query, 0) returns all items, of course if you don't
  /// want data, just don't call IDB's methods.  But our higher level
  /// 'Limit(0, query)' is simply a NoOp.
  let getAll = switch (attribute, limit) {
  | (_, Some(0)) => None
  | (None, None) => Some(range => store->Store.getAllInRange(range))
  | (None, Some(limit)) => Some(range => store->Store.getManyInRange(range, limit))
  | (Some(attribute), None) =>
    if attribute == store->Store.keyPath {
      Some(range => store->Store.getAllInRange(range))
    } else {
      Some(range => store->Store.index(attribute)->Index.getAllInRange(range))
    }
  | (Some(attribute), Some(limit)) =>
    if attribute == store->Store.keyPath {
      Some(range => store->Store.getManyInRange(range, limit))
    } else {
      Some(range => store->Store.index(attribute)->Index.getManyInRange(range, limit))
    }
  }

  switch getAll {
  | Some(fn) => fn(range)->Request.onsuccess(event => event->CursorEvent.result->callback)
  | None => callback([])
  }
}

let _openCursor = (store, attribute, range, direction) => {
  let openCursor = switch attribute {
  | None => (range, direction) => store->Store.openCursor(range, direction)
  | Some(attribute) =>
    if attribute == store->Store.keyPath {
      (range, direction) => store->Store.openCursor(range, direction)
    } else {
      (range, direction) => store->Store.index(attribute)->Index.openCursor(range, direction)
    }
  }
  openCursor(range, direction)
}

let _getOne = (store, attribute, callback, direction) => {
  _openCursor(store, attribute, None, direction)->Request.onsuccess(event => {
    switch event->CursorEvent.cursor->Nullable.toOption {
    | None => callback([])
    | Some(cursor) => callback([cursor->Cursor.value])
    }
  })
}

let _getManyOffset = (store, attribute, range, callback, ~offset: _=?, ~limit: _=?) => {
  let offset = ref(offset)
  let results = []

  _openCursor(store, attribute, range, #next)->Request.onsuccess(event =>
    switch event->CursorEvent.cursor->Nullable.toOption {
    | None => callback(results)
    | Some(cursor) =>
      switch offset.contents {
      | Some(count) => {
          offset := None
          cursor->Cursor.advance(count)
        }

      | None =>
        switch limit {
        | None => {
            results->Array.push(cursor->Cursor.value)
            cursor->Cursor.continue
          }

        | Some(l) =>
          if results->Array.length < l {
            results->Array.push(cursor->Cursor.value)
            cursor->Cursor.continue
          } else {
            callback(results)
          }
        }
      }
    }
  )
}

let _simpleQuery = (store, term, callback, ~offset: _=?, ~limit: _=?) => {
  let (attribute, range) = switch term {
  | All => (None, None)
  | Is(attribute, value) => (Some(attribute), Some(KeyRange.only(value)))
  | NotNull(attribute) => (Some(attribute), None)
  | Lt(attribute, value) => (Some(attribute), Some(KeyRange.upperBound(value, true)))
  | Lte(attribute, value) => (Some(attribute), Some(KeyRange.upperBound(value, false)))
  | Gt(attribute, value) => (Some(attribute), Some(KeyRange.lowerBound(value, true)))
  | Gte(attribute, value) => (Some(attribute), Some(KeyRange.lowerBound(value, false)))
  | Between(attribute, lower, upper) => {
      let (lower_value, lower_exclude) = switch lower {
      | Incl(value) => (value, false)
      | Excl(value) => (value, true)
      }
      let (upper_value, upper_exclude) = switch upper {
      | Incl(value) => (value, false)
      | Excl(value) => (value, true)
      }
      (
        Some(attribute),
        Some(KeyRange.bound(lower_value, upper_value, lower_exclude, upper_exclude)),
      )
    }

  // other cases are habdled before calling this function
  | _ => (None, None)
  }

  switch offset {
  | None | Some(0) => _getAll(store, attribute, range, callback, ~limit?)
  | Some(count) =>
    if count > 0 {
      _getManyOffset(store, attribute, range, callback, ~offset?, ~limit?)
    } else {
      Console.warn2("Ignoring zero or negative offset", offset)
      _getAll(store, attribute, range, callback, ~limit?)
    }
  }
}

let _sortKeys = (items: array<'a>): array<'a> => {
  items->Set.fromArray->Set.toArray->Array.toSorted(IDB.Factory.cmp)
}

let _anyOf = (store, attribute, values, callback, ~offset: _=?, ~limit: _=?) => {
  switch values {
  | [] => callback([])
  | [value] => _simpleQuery(store, Is(attribute, value), callback, ~offset?, ~limit?)
  | values => {
      let results = []
      let values = values->_sortKeys
      let n_values = values->Array.length
      let lower = values->Array.getUnsafe(0)
      let upper = values->Array.getUnsafe(n_values - 1)
      let range = KeyRange.bound(lower, upper, false, false)
      let i = ref(0)
      let offset = ref(offset)
      let collect = cursor => {
        let key = cursor->Cursor.key
        while i.contents < n_values && key > values->Array.getUnsafe(i.contents) {
          i := i.contents + 1
          if i.contents == n_values {
            callback(results)
          }
        }
        if i.contents < n_values {
          let currentValue = values->Array.getUnsafe(i.contents)
          if key == currentValue {
            results->Array.push(cursor->Cursor.value)
            switch limit {
            | None => cursor->Cursor.continue
            | Some(limit) =>
              if results->Array.length < limit {
                cursor->Cursor.continue
              } else {
                callback(results)
              }
            }
          } else {
            cursor->Cursor.continueTo(currentValue)
          }
        }
      }
      _openCursor(store, Some(attribute), Some(range), #next)->Request.onsuccess(event => {
        switch event->CursorEvent.cursor->Nullable.toOption {
        | None => callback(results)
        | Some(cursor) =>
          switch offset.contents {
          | Some(count) => {
              offset := None
              cursor->Cursor.advance(count)
            }

          | None => collect(cursor)
          }
        }
      })
    }
  }
}

let rec _query = (
  store: Store.t,
  expression: read<'a, 'key>,
  callback: array<'a> => unit,
  ~offset: option<int>=?,
  ~limit: option<int>=?,
) => {
  switch expression {
  | NoOp => callback([])
  | Get(id)
  | Is("id", id) => {
      let request = store->Store.get(id)
      let start = switch offset {
      | None => 0
      | Some(count) => count > 0 ? count : 0
      }
      request->Request.onsuccess(_ => {
        switch (start > 0, request->Request.result) {
        | (false, Some(item)) => callback([item])
        | _ => callback([])
        }
      })
    }

  | In(ids) => _query(store, AnyOf("id", ids), callback, ~offset?, ~limit?)
  | NotIn(ids) => _query(store, NoneOf("id", ids), callback, ~offset?, ~limit?)

  | All
  | Is(_, _)
  | NotNull(_)
  | Lt(_, _)
  | Lte(_, _)
  | Gt(_, _)
  | Gte(_, _)
  | Between(_, _, _) =>
    _simpleQuery(store, expression, callback, ~offset?, ~limit?)

  | Min(index) => _getOne(store, Some(index), callback, #next)
  | Max(index) => _getOne(store, Some(index), callback, #prev)

  | AnyOf(index, [val]) => _simpleQuery(store, Is(index, val), callback, ~offset?, ~limit?)
  | AnyOf(index, values) => _anyOf(store, index, values, callback, ~offset?, ~limit?)

  | StartsWith(index, value) =>
    /// See the 'startsWith' in:
    /// https://hacks.mozilla.org/2014/06/breaking-the-borders-of-indexeddb/
    _simpleQuery(
      store,
      Between(index, Incl(value), Incl(value ++ "\uFFFF")),
      callback,
      ~offset?,
      ~limit?,
    )

  | NoneOf(index, values) => {
      let values =
        [None]->Array.concatMany([values->_sortKeys->Array.map(value => Some(value)), [None]])
      let segments = Belt.Array.zip(values, values->Array.sliceToEnd(~start=1))->Array.map(((
        lower,
        upper,
      )) => {
        switch (lower, upper) {
        | (None, Some(upper)) => Lt(index, upper)
        | (Some(lower), Some(upper)) => Between(index, Excl(lower), Excl(upper))
        | (Some(lower), None) => Gt(index, lower)
        | (None, None) => All
        }
      })

      let results = []

      /// We cannot push down the offset to the segments, because we wouldn't
      /// know how many items were skipped per segment.  So we still collect
      /// the values, possibly extending the limit to account for the offset.
      let offset = switch offset {
      | None => 0
      | Some(count) => count > 0 ? count : 0
      }
      let extendedLimit = limit->Option.map(l => l + offset)
      let rec next = () =>
        switch segments->Array.shift {
        | Some(expression) => _simpleQuery(store, expression, gather, ~limit=?extendedLimit)
        | None => callback(offset > 0 ? results->Array.sliceToEnd(~start=offset) : results)
        }
      and gather = items => {
        let accepted = switch extendedLimit {
        | None => items
        | Some(limit) => {
            let many = limit - results->Array.length
            if many > 0 {
              items->Array.slice(~start=0, ~end=many)
            } else {
              []
            }
          }
        }
        results->Array.pushMany(accepted)
        next()
      }
      next()
    }

  | And(left, right) => {
      let results = ref([])
      let gather = items => {
        switch results.contents {
        | [] => results := [items]
        | [prevResults] => {
            let itemIds = items->Array.map(item => item["id"])->Set.fromArray
            let res = prevResults->Array.filter(item => itemIds->Set.has(item["id"]))
            let start = switch offset {
            | None => 0
            | Some(count) => count > 0 ? count : 0
            }
            callback(
              switch limit {
              | None => start > 0 ? res->Array.sliceToEnd(~start) : res
              | Some(limit) => res->Array.slice(~start, ~end=limit)
              },
            )
          }

        | _ => () // this case will never happen
        }
      }

      /// FIXME: We cannot limit the underlying queries, and can only limit
      /// the whole result.  In order to fix this, we would need a more
      /// sophisticated query executor.
      _query(store, left, gather)
      _query(store, right, gather)
    }

  | Or(left, right) => {
      let results = Dict.make()
      let awaiting = ref(2)
      let gather = items => {
        items->Array.forEach(item => {results->Dict.set(item["id"], item)})
        awaiting := awaiting.contents - 1
        if awaiting.contents == 0 {
          let res = results->Dict.valuesToArray
          let start = switch offset {
          | None => 0
          | Some(count) => count > 0 ? count : 0
          }
          callback(
            switch limit {
            | None => start > 0 ? res->Array.sliceToEnd(~start) : res
            | Some(limit) => res->Array.slice(~start, ~end=limit)
            },
          )
        }
      }

      /// FIXME: We cannot limit the underlying queries, and can only limit
      /// the whole result.  In order to fix this, we would need a more
      /// sophisticated query executor.
      _query(store, left, gather)
      _query(store, right, gather)
    }

  | Offset(offset, read) =>
    if offset > 0 {
      _query(store, read, callback, ~offset, ~limit?)
    } else {
      _query(store, read, callback, ~limit?)
    }
  | Limit(l, q) =>
    if l > 0 {
      let l = limit->Belt.Option.map(limit => l < limit ? l : limit)->Belt.Option.getWithDefault(l)
      _query(store, q, callback, ~offset?, ~limit=l)
    } else {
      _query(store, NoOp, callback, ~offset?)
    }
  }
}

let _simplifyReadRequest = readRequest =>
  readRequest
  ->Js.Dict.entries
  ->Belt.Array.keepMap(((name, expression)) =>
    switch expression {
    | NoOp => None
    | _ => Some((name, expression))
    }
  )
  ->Js.Dict.fromArray

let _simplifyWriteRequest = writeRequest =>
  writeRequest
  ->Js.Dict.entries
  ->Belt.Array.keepMap(((name, commands)) =>
    switch commands {
    | [] => None
    | commands => Some((name, commands))
    }
  )
  ->Js.Dict.fromArray

let _openTransaction = (db, storeNames, mode, ~transaction: option<Transaction.t<'db>>=?) => {
  let iOwnTheTransaction = transaction->Option.isNone
  let transaction = switch transaction {
  | None => db->Database.transaction(storeNames, mode)
  | Some(transaction) => transaction
  }
  (iOwnTheTransaction, transaction)
}

let _read = (db, readRequest, response, callback, ~transaction: option<Transaction.t<'db>>=?) => {
  let readRequest = readRequest->_simplifyReadRequest
  switch readRequest->Dict.keysToArray {
  | [] => callback(response)
  | storeNames => {
      let toSolve = storeNames->Array.length
      let solved = ref(0)

      let (iOwnTheTransaction, transaction) = _openTransaction(
        db,
        storeNames,
        #readonly,
        ~transaction?,
      )

      readRequest
      ->Js.Dict.entries
      ->Belt.Array.forEach(((storeName, expression)) => {
        let store = transaction->Transaction.objectStore(storeName)
        _query(store, expression, results => {
          response->Dict.set(storeName, results)
          solved := solved.contents + 1
          if solved.contents == toSolve {
            if iOwnTheTransaction {
              transaction->Transaction.commit
            }
            callback(response)
          }
        })
      })
    }
  }
}

let _write = (db, writeRequest, response, callback, ~transaction: option<Transaction.t<'db>>=?) => {
  let writeRequest = writeRequest->_simplifyWriteRequest
  switch writeRequest->Dict.keysToArray {
  | [] => callback(response)
  | storeNames => {
      let (iOwnTheTransaction, transaction) = _openTransaction(
        ~transaction?,
        db,
        storeNames,
        #readwrite,
      )
      writeRequest
      ->Js.Dict.entries
      ->Array.forEach(((storeName, commands)) => {
        let store = transaction->Transaction.objectStore(storeName)
        commands->Array.forEach(command => {
          switch command {
          // We have experimentally tested these '->ignore' are safe writing
          // transactions. We don't have to wait till the previous action's
          // IDBRequest triggers success to trigger the next action.
          | Clear => store->Store.clear->ignore
          | Save(item) => store->Store.put(item)->ignore
          | Delete(id) => store->Store.delete(id)->ignore
          }
        })
      })
      if iOwnTheTransaction {
        transaction->Transaction.commit
      }
      callback(response)
    }
  }
}

let rec _executeQueries = (~transaction: Transaction.t<'db>, db, queries, response, callback) => {
  switch queries {
  | list{} => callback(response)
  | list{query, ...queries} =>
    switch query {
    | Read(q) =>
      db->_read(~transaction, response->q, response, response => {
        db->_executeQueries(~transaction, queries, response, callback)
      })

    | Write(q) =>
      db->_write(~transaction, response->q, response, response => {
        db->_executeQueries(~transaction, queries, response, callback)
      })
    }
  }
}

let _makeResponse = db => {
  db->Database.objectStoreNames->Belt.Array.map(name => (name, []))->Dict.fromArray
}

let read = (db: Database.t, readRequest) => {
  let response = db->_makeResponse
  Core.Promise.make((resolve, reject) => {
    reject->ignore
    db->_read(readRequest, response, response => resolve(response))
  })
}

let write = (db: Database.t, writeRequest) => {
  let response = db->_makeResponse
  Core.Promise.make((resolve, reject) => {
    reject->ignore
    db->_write(writeRequest, response, response => resolve(response))
  })
}

let do = (db: Database.t, queries: array<query<'a, 'key>>) => {
  let response = db->_makeResponse
  let isWrite = queries->Belt.Array.some(q =>
    switch q {
    | Write(_) => true
    | Read(_) => false
    }
  )
  Core.Promise.make((resolve, reject) => {
    reject->ignore

    let transaction =
      db->Database.transaction(db->Database.objectStoreNames, isWrite ? #readwrite : #readonly)

    db->_executeQueries(~transaction, queries->List.fromArray, response, response => {
      transaction->Transaction.commit
      resolve(response)
    })
  })
}
