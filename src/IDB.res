@@uncurried
module Core = RescriptCore

module UpdateRequest = IDB__UpdateRequest
module DeleteRequest = IDB__DeleteRequest
module CountRequest = IDB__CountRequest

module Cursor = IDB__Cursor
module CursorEvent = IDB__CursorEvent
module Request = IDB__Request
module KeyRange = IDB__KeyRange

type direction = IDB__Index.direction
module Index = IDB__Index

module Store = IDB__Store
module Migration = IDB__Migration
module Transaction = IDB__Transaction
module Database = IDB__Database

module Utils = IDB__Utils
module Factory = IDB__Factory
