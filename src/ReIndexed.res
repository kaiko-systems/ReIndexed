@@uncurried
open RescriptCore

external value: 'a => string = "%identity"

module type ModelT = {
  type t
  type index
}

type value = ReIndexed_Transaction.value
type bound = ReIndexed_Transaction.bound

@doc("The exported interface for any Identifier module.

Notice that IT'S REQUIRED that the type `t` be just an opaque string-backed
data.  If you pass any other type, you just want to the see the world burn.
")
module type Identifier = {
  type t
  let fromString: string => t
  let toString: t => string
  let manyFromString: array<string> => array<t>
  let manyToString: array<t> => array<string>
}

module type Types = {
  type t
  type id
  type index
  type value

  type rec read =
    | NoOp
    | Get(id)
    // index queries
    | All
    | In(array<id>)
    | NotIn(array<id>)
    | Is(index, value)
    | NotNull(index)
    | Lt(index, value)
    | Lte(index, value)
    | Gt(index, value)
    | Gte(index, value)
    | Between(index, bound, bound)
    | AnyOf(index, array<value>)
    | NoneOf(index, array<value>)
    | StartsWith(index, string)
    | Max(index) // gets the record for which the value in the index is max
    | Min(index) // gets the record for which the value in the index is min
    // recursive queries
    | And(read, read)
    | Or(read, read)
    | Limit(int, read)
    | Offset(int, read)

  type write =
    | Clear
    | Save(t)
    | Delete(id)

  let clear: unit => write
  let save: t => write
  let delete: id => write
  let remove: t => write
}

module MakeTypes = (
  T: {
    type t
    type id
    type index
    type value
  },
): (
  Types with type t := T.t and type id := T.id and type value := T.value and type index := T.index
) => {
  include T

  type rec read =
    | NoOp
    | Get(T.id)
    // index queries
    | All
    | In(array<T.id>)
    | NotIn(array<T.id>)
    | Is(T.index, T.value)
    | NotNull(T.index)
    | Lt(T.index, T.value)
    | Lte(T.index, T.value)
    | Gt(T.index, T.value)
    | Gte(T.index, T.value)
    | Between(T.index, bound, bound)
    | AnyOf(T.index, array<T.value>)
    | NoneOf(T.index, array<T.value>)
    | StartsWith(T.index, string)
    | Max(T.index) // gets the record for which the value in the index is max
    | Min(T.index) // gets the record for which the value in the index is min
    // recursive queries
    | And(read, read)
    | Or(read, read)
    // Limit & offset
    | Limit(int, read)
    | Offset(int, read)

  type write =
    | Clear
    | Save(T.t)
    | Delete(T.id)

  let clear = _ => Clear
  let save = record => Save(record)
  let delete = id => Delete(id)
  let remove = record => Delete((record->Obj.magic)["id"])
}

module type Model = {
  include Types

  type key = t => value
  type actions = array<write>

  module IdComparator: Belt.Id.Comparable with type t = t
  module Id: Identifier with type t = id
}

module MakeIdModel = (
  Model: ModelT,
  I: {
    type t
  },
): (
  Model with type t = Model.t and type id = I.t and type index = Model.index and type value = value
) => {
  module Id: Identifier with type t = I.t = {
    include I
    external fromString: string => t = "%identity"
    external toString: t => string = "%identity"
    external manyFromString: array<string> => array<t> = "%identity"
    external manyToString: array<t> => array<string> = "%identity"
  }
  module Types = MakeTypes({
    type t = Model.t
    type id = Id.t
    type index = Model.index
    type value = value
  })

  module IdComparator = Belt.Id.MakeComparableU({
    type t = Model.t
    let cmp: (t, t) => int = (obj1, obj2) =>
      Pervasives.compare((obj1->Obj.magic)["id"], (obj2->Obj.magic)["id"])
  })

  include Model
  include Types
  type id = Id.t
  type value = value
  type key = t => value
  type actions = array<write>
}

module MakeModel = (Model: ModelT) => MakeIdModel(
  Model,
  {
    type t = string
  },
)

module type DatabaseT = {
  let migrations: unit => array<IDB.Migration.t>
}

%%private(
  @doc("Run each function in turn until one resolves to an Error, or all resolve to Ok.")
  let rec _bail = async (items: array<'a>, fns: list<unit => promise<result<'a, 'e>>>): result<
    array<'a>,
    'e,
  > =>
    switch fns {
    | list{} => Ok(items)
    | list{fn, ...fns} =>
      let res = await fn()
      switch res {
      | Ok(item) => await _bail(items->Array.concat([item]), fns)
      | Error(msg) => Error(msg)
      }
    }
  and bail = (fns: array<unit => promise<result<'a, 'e>>>): promise<result<array<'a>, 'e>> =>
    _bail([], fns->List.fromArray)
)

@doc("Creates a DB connector, you can open many connections to the same DB and
      use, MakeUnboundQuery to create ORM-like operations on those DB
      connections.")
module MakeDBConnector = (Database: DatabaseT) => {
  include Database

  @doc("Disconnect to the DB.  If not connected, do nothing.  ")
  let disconnect = db => {
    db->IDB.Database.close
  }

  @doc("Disconnects and drop the DB.  Notice that if there are opened connections
        to the DB (in other tabs, for instance), the operation might take a while.")
  let drop: IDB.Database.t => promise<result<unit, exn>> = db => {
    let dbname = db->IDB.Database.name
    disconnect(db)
    IDB__Utils.dropdb(dbname)
  }

  @doc("Connect to the DB and run missing migrations.

  Resolves to Ok(db) after all migrations are done and if all migrations
  returned Ok.  If any migration returns a Error, resolve to 'Error((index, error,
  db))'.

  ")
  let connect: string => promise<result<IDB.Database.t, _>> = dbname => {
    let migrations = Database.migrations()
    let upgrade = (db, oldVersion, newVersion, transaction) => {
      Console.debug4("Migrating DB " ++ dbname ++ " from version", oldVersion, "to", newVersion)

      let prepared =
        migrations
        ->Array.mapWithIndex((migration, index) => (index, migration))
        ->Array.sliceToEnd(~start=oldVersion)
        ->Array.map(((index, m)) => (index, m(index)))

      let migrations = prepared->Array.map(((index, migration)) => () => {
        Console.debug2("Running migration ", index)
        migration(db, transaction)
        ->Promise.catch(async e => {
          Console.error2("Error in migration", e)
          Error(`Error in migration ${index->Int.toString}`)
        })
        ->Promise.then(async res =>
          switch res {
          | v => v
          | exception e => {
              Console.error2("Error in migration", e)
              Error(`Error in migration ${index->Int.toString}`)
            }
          }
        )
      })

      bail(migrations)
      ->Promise.then(async res =>
        switch res {
        | Ok(_) => Ok()
        | Error(e) => Error(e)
        }
      )
      ->Promise.catch(async _ => {
        transaction->IDB.Migration.Transaction.abort
        Error("Migrations failed")
      })
    }

    IDB.Migration.Database.connect(dbname, migrations->Array.length, upgrade)
    ->Promise.then(async res =>
      switch res {
      | Ok(db) => {
          let db = db->IDB.Migration.Database.downcast
          Ok(db)
        }

      | Error(e) => {
          Console.error3("Could not connect to the DB.", dbname, e)
          Error(e)
        }
      }
    )
    ->Promise.catch(async e => Error(e))
  }
}

/// Higher level API

type query<'response, 'read, 'write> =
  | Read('response => 'read)
  | Write('response => 'write)

module type QueryDefinition = {
  type read
  type write
  type response

  let makeRead: unit => read
  let makeWrite: unit => write
  let makeResponse: unit => response
}

module type UnboundQuery = {
  type read
  type write
  type response
  type query

  let makeRead: unit => read
  let makeWrite: unit => write
  let makeResponse: unit => response

  let read: (IDB.Database.t, read) => promise<response>
  let write: (IDB.Database.t, write) => promise<response>
  let do: (IDB.Database.t, array<query>) => promise<response>

  module Unsafe: {
    let read: (IDB.Database.t, read) => promise<response>
    let write: (IDB.Database.t, write) => promise<response>
    let do: (IDB.Database.t, array<query>) => promise<response>
  }

  module Safe: {
    let read: (IDB.Database.t, read) => promise<result<response, exn>>
    let write: (IDB.Database.t, write) => promise<result<response, exn>>
    let do: (IDB.Database.t, array<query>) => promise<result<response, exn>>
  }

  let value: 'a => string // FIXME
}

module MakeUnboundQuery = (Q: QueryDefinition): (
  UnboundQuery
    with type response = Q.response
    and type read = Q.read
    and type write = Q.write
    and type query = query<Q.response, Q.read, Q.write>
) => {
  include Q
  type query = query<response, read, write>

  %%private(
    external _readToIR: read => Dict.t<ReIndexed_Transaction.read<'a, 'key>> = "%identity"
    external _writeToIR: write => Dict.t<array<ReIndexed_Transaction.write<'a>>> = "%identity"
    external _responseIRToResponse: ReIndexed_Transaction.response<'a> => response = "%identity"
    external _responseToResponseIR: response => ReIndexed_Transaction.response<'a> = "%identity"

    let _queriesToIR = queries => {
      queries->Array.map((query): ReIndexed_Transaction.query<'a, 'key> =>
        switch query {
        | Read(f) => Read(response => response->_responseIRToResponse->f->_readToIR)
        | Write(f) => Write(response => response->_responseIRToResponse->f->_writeToIR)
        }
      )
    }
  )
  external value: 'a => string = "%identity" // FIXME

  module Unsafe = {
    @doc("Perform the read in a single DB transaction.")
    let read = (db, read: read): promise<response> =>
      db
      ->ReIndexed_Transaction.read(read->_readToIR)
      ->Promise.then(async r => r->_responseIRToResponse)

    @doc("Perform the write in a single DB transaction.

      The transaction will only lock the stores involved in the write, i.e the
      stories with non empty set of actions.")
    let write = (db, write: write): promise<response> =>
      db
      ->ReIndexed_Transaction.write(write->_writeToIR)
      ->Promise.then(async r => r->_responseIRToResponse)

    @doc("Perform all the actions in a single DB transaction.

      If all the actions are `Read`, the transaction is created in #readonly
      mode, so it doesn't need a lock to the stores.  However, if at least on
      action is a Write, the ENTIRE database is locked for the duration of the
      whole transaction.")
    let do = (db, queries: array<query>): promise<response> => {
      db
      ->ReIndexed_Transaction.do(queries->_queriesToIR)
      ->Promise.then(async r => r->_responseIRToResponse)
    }
  }

  module Safe = {
    @doc("Perform the read in a single DB transaction.")
    let read = (db, read: read): promise<result<response, exn>> =>
      db
      ->ReIndexed_Transaction.read(read->_readToIR)
      ->Promise.then(async response => Ok(response->_responseIRToResponse))
      ->Promise.catch(async error => Error(error))

    @doc("Perform the write in a single DB transaction.

      The transaction will only lock the stores involved in the write, i.e the
      stories with non empty set of actions.")
    let write = (db, write: write): promise<result<response, exn>> =>
      db
      ->ReIndexed_Transaction.write(write->_writeToIR)
      ->Promise.then(async response => Ok(response->_responseIRToResponse))
      ->Promise.catch(async error => Error(error))

    @doc("Perform all the actions in a single DB transaction.

      If all the actions are `Read`, the transaction is created in #readonly
      mode, so it doesn't need a lock to the stores.  However, if at least on
      action is a Write, the ENTIRE database is locked for the duration of the
      whole transaction.")
    let do = (db, queries: array<query>): promise<result<response, exn>> => {
      db
      ->ReIndexed_Transaction.do(queries->_queriesToIR)
      ->Promise.then(async response => Ok(response->_responseIRToResponse))
      ->Promise.catch(async error => Error(error))
    }
  }

  let read = Unsafe.read
  let write = Unsafe.write
  let do = Unsafe.do
}

module type BoundQuery = {
  type read
  type write
  type response
  type query

  let makeRead: unit => read
  let makeWrite: unit => write
  let makeResponse: unit => response

  let read: read => promise<response>
  let write: write => promise<response>
  let do: array<query> => promise<response>

  module Unsafe: {
    let read: read => promise<response>
    let write: write => promise<response>
    let do: array<query> => promise<response>
  }

  module Safe: {
    let read: read => promise<result<response, exn>>
    let write: write => promise<result<response, exn>>
    let do: array<query> => promise<result<response, exn>>
  }

  let value: 'a => string // FIXME
}

/// TODO: Provide the module type Database with the right type constraints.
module MakeDatabase = (Database: DatabaseT) => {
  // database & migrations
  include Database

  type connection = {
    mutable db: option<IDB.Database.t>,
    mutable failed: bool,
  }
  let connection = {db: None, failed: false}

  module Connector = MakeDBConnector(Database)

  @doc("Disconnect to the DB.  If not connected, do nothing.  ")
  let disconnect = () => {
    switch connection.db {
    | None => ()
    | Some(db) => db->Connector.disconnect
    }

    connection.db = None
  }

  @doc("Disconnects and drop the DB.  Notice that if there are opened connections
        to the DB (in other tabs, for instance), the operation might take a while.")
  let drop: unit => promise<result<unit, exn>> = async () => {
    switch connection.db {
    | Some(db) => {
        Console.debug2(__MODULE__, `Dropping DB ${db->IDB.Database.name}`)
        connection.db = None
        await db->Connector.drop
      }

    | None => Ok()
    }
  }

  @doc("Connect to the DB and run missing migrations.

  Resolves to Ok(db) after all migrations are done and if all migrations
  returned Ok.  If any migration returns a Error, resolve to 'Error((index, error,
  db))'.

  ")
  let connect: string => promise<result<IDB.Database.t, _>> = dbname => {
    Console.debug2("Connecting", dbname)
    Connector.connect(dbname)->Promise.then(async result =>
      switch result {
      | Ok(db) => {
          connection.db = Some(db)
          Ok(db)
        }

      | Error(e) => {
          Console.error2("Could not connect", e)
          Error(e)
        }
      }
    )
  }

  @doc("Low level patterns to DB.

  This is are basic patterns to read and write in the DB using more lower
  primitives than the ones provided by the MakeQuery functor.

  Contents:

  - `MakeConsumer`; allows to read one record at a time from a single store.
  - `MakeWriter`; allows to write many records in a single table.

  ")
  module Patterns = {
    @doc("Open a transaction to the given stores in a given mode.")
    let transaction = (storeNames, mode) =>
      switch connection.db->Option.map(IDB.Database.transaction(_, storeNames, mode)) {
      | Some(t) => Ok(t)
      | None => Error("The DB connection is not established.")
      }

    include ReIndexedPatterns
  }

  module MakeQuery = (Q: QueryDefinition): (
    BoundQuery
      with type response = Q.response
      and type read = Q.read
      and type write = Q.write
      and type query = query<Q.response, Q.read, Q.write>
  ) => {
    include Q
    type query = query<response, read, write>
    module Impl = MakeUnboundQuery(Q)
    let value = Impl.value

    let _withDb = f =>
      switch connection.db {
      | Some(db) => db->f
      | None =>
        Promise.make((resolve, reject) => {
          resolve->ignore
          reject(Js.Exn.raiseError("The database is not connected"))
        })
      }

    module Unsafe = {
      @doc("Perform the read in a single DB transaction.")
      let read = (read: read): promise<response> => _withDb(db => db->Impl.Unsafe.read(read))

      @doc("Perform the write in a single DB transaction.

      The transaction will only lock the stores involved in the write, i.e the
      stories with non empty set of actions.")
      let write = (write: write): promise<response> => _withDb(db => db->Impl.Unsafe.write(write))

      @doc("Perform all the actions in a single DB transaction.

      If all the actions are `Read`, the transaction is created in #readonly
      mode, so it doesn't need a lock to the stores.  However, if at least on
      action is a Write, the ENTIRE database is locked for the duration of the
      whole transaction.")
      let do = (queries: array<query>): promise<response> => {
        _withDb(db => db->Impl.Unsafe.do(queries))
      }
    }

    module Safe = {
      @doc("Perform the read in a single DB transaction.")
      let read = (read: read): promise<result<response, exn>> =>
        _withDb(db => db->Impl.Safe.read(read))

      @doc("Perform the write in a single DB transaction.

      The transaction will only lock the stores involved in the write, i.e the
      stories with non empty set of actions.")
      let write = (write: write): promise<result<response, exn>> =>
        _withDb(db => db->Impl.Safe.write(write))

      @doc("Perform all the actions in a single DB transaction.

      If all the actions are `Read`, the transaction is created in #readonly
      mode, so it doesn't need a lock to the stores.  However, if at least on
      action is a Write, the ENTIRE database is locked for the duration of the
      whole transaction.")
      let do = (queries: array<query>): promise<result<response, exn>> => {
        _withDb(db => db->Impl.Safe.do(queries))
      }
    }

    let read = Unsafe.read
    let write = Unsafe.write
    let do = Unsafe.do
  }
}
