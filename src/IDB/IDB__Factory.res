@@uncurried

// The type of the 'indexedDB' global.
// https://developer.mozilla.org/en-US/docs/Web/API/IDBFactory

type db = {name: string, version: int}
@scope("indexedDB") external databases: unit => promise<db> = "databases"

@val @scope("indexedDB") external deleteDatabase: string => IDB__OpenDBRequest.t = "deleteDatabase"
@val @scope("indexedDB") external cmp: ('a, 'a) => RescriptCore.Ordering.t = "cmp"
