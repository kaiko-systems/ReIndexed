@@uncurried

module MakeStore = () => {
  type t
  @get external keyPath: t => string = "keyPath"
  @get external name: t => string = "name"
  @set external setName: (t, string) => unit = "name"

  @send external getAll: t => IDB__Request.t = "getAll"
  @send external getMany: (t, int) => IDB__Request.t = "getAll"
  @send external getAllInRange: (t, option<IDB__KeyRange.t>) => IDB__Request.t = "getAll"
  @send external getManyInRange: (t, option<IDB__KeyRange.t>, int) => IDB__Request.t = "getAll"

  @send external add: (t, 'a) => IDB__Request.t = "add"
  @send external put: (t, 'a) => IDB__Request.t = "put"
  @send external get: (t, string) => IDB__Request.t = "get"
  @send external delete: (t, string) => IDB__Request.t = "delete"
  @send external clear: t => IDB__Request.t = "clear"

  @send external index: (t, 'index) => IDB__Index.t = "index"

  @send
  external openCursor: (t, option<IDB__KeyRange.t>, IDB__Index.direction) => IDB__Request.t =
    "openCursor"

  @send
  external openKeyCursor: (t, option<IDB__KeyRange.t>, IDB__Index.direction) => IDB__Request.t =
    "openKeyCursor"

  @send external count: t => IDB__CountRequest.t = "count"
  @send external countKeyRange: (t, IDB__KeyRange.t) => IDB__CountRequest.t = "count"
}

include MakeStore()
