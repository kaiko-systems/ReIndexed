@@uncurried

module MakeCoreDatabase = () => {
  type t
  type transactionMode = [#readwrite | #readonly]

  @send
  external transaction: (t, array<string>, transactionMode) => IDB__Transaction.t<t> = "transaction"
  @send external close: t => unit = "close"

  @get external name: t => string = "name"
  @get external objectStoreNames: t => array<string> = "objectStoreNames"
}

include MakeCoreDatabase()
