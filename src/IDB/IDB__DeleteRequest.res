@@uncurried

/// The result of calling 'Cursor.delete'.
///
/// [1]: https://developer.mozilla.org/en-US/docs/Web/API/IDBCursor/delete
///
type t
@set external onsuccess: (t, unit => unit) => unit = "onsuccess"

/// According to [1], the result attribute is set to undefined.  We simply
/// return None.
let result = (_x: t): option<'key> => None
