@@uncurried
open RescriptCore

module Internal = IDB__Request.MakeIDBRequest({
  type cursor = IDB__Event.t
})

include Internal
@get external result: t => option<int> = "result"

let promise = (request: t): promise<int> =>
  Promise.make((resolve, reject) => {
    request->onerror(_ => reject(request->error))
    request->onsuccess(_ => resolve(request->result->Belt.Option.getWithDefault(0)))
  })
