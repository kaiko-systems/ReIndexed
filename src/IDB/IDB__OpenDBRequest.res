@@uncurried

type t
@val @scope("indexedDB") external make: (string, int) => t = "open"

@set external onsuccess: (t, IDB__OpenedDBEvent.t<t> => unit) => unit = "onsuccess"
@set external onerror: (t, IDB__OpenedDBEvent.t<t> => unit) => unit = "onerror"
@get external result: t => option<'a> = "result"

@set external onupgradeneeded: (t, IDB__VersionChangeEvent.t<t> => unit) => unit = "onupgradeneeded"
@get external transaction: t => option<IDB__Migration__Transaction.t<'db>> = "transaction"

@get external error: t => option<exn> = "error"
