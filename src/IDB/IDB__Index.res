@@uncurried

type direction = [#next | #nextunique | #prev | #prevunique]
type t

@send external openCursor: (t, option<IDB__KeyRange.t>, direction) => IDB__Request.t = "openCursor"

@send
external openKeyCursor: (t, option<IDB__KeyRange.t>, direction) => IDB__Request.t = "openKeyCursor"

@send external getAll: t => IDB__Request.t = "getAll"
@send external getMany: (t, int) => IDB__Request.t = "getAll"
@send external getAllInRange: (t, option<IDB__KeyRange.t>) => IDB__Request.t = "getAll"
@send external getManyInRange: (t, option<IDB__KeyRange.t>, int) => IDB__Request.t = "getAll"

@send external count: t => IDB__CountRequest.t = "count"
@send external countKeyRange: (t, IDB__KeyRange.t) => IDB__CountRequest.t = "count"
