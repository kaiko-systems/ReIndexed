@@uncurried

type t<'request>
@get @scope("target") external result: t<'r> => 'a = "result"
@get @scope("target") external error: t<'r> => option<'a> = "error"
@get external target: t<'r> => 'r = "target"
