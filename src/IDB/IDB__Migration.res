@@uncurried

module Store = IDB__Migration__Store
module Transaction = IDB__Migration__Transaction
module Database = IDB__Migration__Database
module Utils = IDB__Migration__Utils

@doc("Migrations will be executed in two phases.

First, the migration function is call passing only the index of the migration.
This allows the migration to setup feedback if required.

Then each migration is called in turn with the DB and the transaction.  If any
migration returns an Error, it is logged in the console and the transaction is
aborted. ")
type t = int => (Database.t, Transaction.t<Database.t>) => promise<result<unit, string>>
