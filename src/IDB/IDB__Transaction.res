@@uncurried

module MakeTransactionModule = () => {
  type t<'db>

  @send external objectStore: (t<'db>, string) => IDB__Store.t = "objectStore"
  @set external oncomplete: (t<'db>, unit => unit) => unit = "oncomplete"

  %%private(external _commit: t<'db> => unit = "commit")

  let commit: t<'db> => unit = transaction => {
    /// Don't fail in older version of browsers:
    /// https://developer.mozilla.org/en-US/docs/Web/API/IDBTransaction/commit#browser_compatibility
    /// Some browsers have a 'commit' property that is not a function:
    /// SURVEY-TOOL-FY, https://kaiko-systems-gmbh-hb.sentry.io/share/issue/2e3673c97e8d4121908a00615c4d53f4/
    try {
      transaction->_commit
    } catch {
    | _ => ()
    }
  }

  @send external abort: t<'db> => unit = "abort"
  @send external db: t<'db> => 'db = "db"

  type mode = [#readonly | #readwrite | #versionchange]
  @get external mode: t<'db> => mode = "mode"
}

include MakeTransactionModule()
