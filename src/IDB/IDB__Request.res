@@uncurried
open RescriptCore

module MakeIDBRequest = (
  T: {
    type cursor
  },
) => {
  type t
  @set external onsuccess: (t, T.cursor => unit) => unit = "onsuccess"
  @get external result: t => option<'a> = "result"

  @set external onerror: (t, IDB__Event.t => unit) => unit = "onerror"
  @get external error: t => 'a = "error"

  let promise = (request: t): promise<option<'a>> =>
    Promise.make((resolve, reject) => {
      request->onerror(_ => reject(request->error))
      request->onsuccess(_ => resolve(request->result))
    })
}

include MakeIDBRequest({
  type cursor = IDB__CursorEvent.t
})
