@@uncurried

/// The result of calling 'Cursor.update'.
///
/// [1]: https://developer.mozilla.org/en-US/docs/Web/API/IDBCursor/update
/// [2]: https://w3c.github.io/IndexedDB/#ref-for-dom-idbcursor-update%E2%91%A0
///
type t
@set external onsuccess: (t, unit => unit) => unit = "onsuccess"

/// [2] says the result is the object's key.
@get external result: t => 'key = "result"
