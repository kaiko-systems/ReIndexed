@@uncurried

include IDB__OpenedDBEvent

@get external oldVersion: t<'request> => int = "oldVersion"
@get external newVersion: t<'request> => int = "newVersion"
