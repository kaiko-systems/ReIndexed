@@uncurried
open RescriptCore

type t
@get @scope("target") external cursor: t => Nullable.t<IDB__Cursor.t> = "result"
@get @scope("target") external result: t => array<'a> = "result"
