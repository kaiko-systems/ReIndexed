@@uncurried

include IDB__Transaction.MakeTransactionModule()

@send external objectStore: (t<'db>, string) => IDB__Migration__Store.t = "objectStore"

external downcast: t<'db> => IDB__Transaction.t<'db> = "%identity"
