@@uncurried
open RescriptCore

include IDB__Database.MakeCoreDatabase()
external downcast: t => IDB__Database.t = "%identity"

let createObjectStore: (t, string) => IDB__Migration__Store.t = %raw(`
 function (database, name) {
    return database.createObjectStore(name, {"keyPath": "id"});
 }
`)

@send external deleteObjectStore: (t, string) => unit = "deleteObjectStore"

type stage = Opening | Migrating | MigrationsDone | Done | Failed(string)

exception DBError(string, option<exn>)
exception DBMigrationError(string, t)
exception DBOpenError(string, exn)

let connect: (
  string,
  int,
  (t, int, int, IDB__Migration__Transaction.t<t>) => promise<result<_, _>>,
) => promise<result<t, _>> = (name, version, upgrade) =>
  Promise.make((resolve, reject) => {
    /// When we request the DB we cannot know if it's going to run migrations
    /// or not.  We need to ensure that migrations are done before resolving
    /// the resulting promise.  The state should allow to go from Opening
    /// directly to Done, or go first through Migrating, then MigrationsDone
    /// and finally Done.  Failed can be reached from any other state.
    let state = ref(Opening)
    let ondone = db => {
      switch state.contents {
      | MigrationsDone | Opening => {
          state := Done
          resolve(db)
        }

      | Migrating => () // Wait till migrations are done.
      | Done => () // Already resolved, so nothing to do
      | Failed(name) => reject(DBMigrationError(name, db))
      }
    }

    switch IDB__OpenDBRequest.make(name, version) {
    | request =>
      request->IDB__OpenDBRequest.onupgradeneeded(event => {
        let db = event->IDB__VersionChangeEvent.result
        let transaction = request->IDB__OpenDBRequest.transaction->Option.getUnsafe
        state := Migrating
        transaction->IDB__Migration__Transaction.oncomplete(_ => ondone(db))
        upgrade(db, event->IDB__VersionChangeEvent.oldVersion, version, transaction)
        ->Promise.then(
          async res => {
            state :=
              switch res {
              | Ok(_) => MigrationsDone
              | Error(msg) => Failed(msg)
              }
            ()
          },
        )
        ->ignore
      })

      request->IDB__OpenDBRequest.onsuccess(event => {
        let db = event->IDB__OpenedDBEvent.result
        ondone(db)
      })
      request->IDB__OpenDBRequest.onerror(e => {
        state := Failed("Could not open the DB")
        reject(DBError("Could not open the DB", e->IDB__OpenedDBEvent.error))
      })

    | exception e => reject(DBOpenError(name, e))
    }
  })
  ->Promise.then(async db => Ok(db))
  ->Promise.catch(async e => Error(e))

@doc(" Creates a store with a `name` in the given `db` and calls
`createManyIndexes` afterwards.")
let createStandardStore = (
  db: t,
  name: string,
  indexes: array<string>,
): IDB__Migration__Store.t => {
  db->createObjectStore(name)->IDB__Migration__Store.createManyIndexes(indexes)
}
