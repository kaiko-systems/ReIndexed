@@uncurried
open RescriptCore

include IDB__Store.MakeStore()

external downcast: t => IDB__Store.t = "%identity"

@send external deleteIndex: (t, string) => unit = "deleteIndex"
@send external createIndex: (t, string, string) => unit = "createIndex"

let createSimpleIndex = (store, name) => store->createIndex(name, name)

let createUniqueIndex: (t, string, array<string>) => unit = %raw(`
      function (store, name, attrPath) {
        store.createIndex(name, attrPath, {unique: true});
      }
    `)
let createMultiEntryIndex: (t, string, string) => unit = %raw(`
      function (store, name, attrPath) {
        store.createIndex(name, attrPath, {multiEntry: true});
      }
    `)

let createSimpleMultiEntryIndex = (store, name) => store->createMultiEntryIndex(name, name)

let createManyIndexes = (obj: t, indexes: array<string>): t => {
  indexes->Array.forEach(index => obj->createSimpleIndex(index))
  obj
}
