@@uncurried

@deprecated
let createManyIndexes = (obj, indexes) => obj->IDB__Migration__Store.createManyIndexes(indexes)

@deprecated
let createStandardStore = (db, name, indexes) =>
  db->IDB__Migration__Database.createStandardStore(name, indexes)

/// APIs which are targeted to migrations.
module type Reader = {
  type t
  let storeName: string
  let read: IDB__Migration__Transaction.t<'db> => promise<array<t>>
}

module MakeReader = (
  R: {
    type t
    let storeName: string
  },
): (Reader with type t = R.t) => {
  module Impl = ReIndexedPatterns.MakePagedReader({
    include R
    let offset = 0
    let limit = None
  })
  include R
  let read = transaction => Impl.read(transaction->IDB__Migration__Transaction.downcast)
}

module type Writer = {
  type t
  let storeName: string
  let data: unit => array<t>
  let write: (IDB__Migration__Transaction.t<'db>, bool) => promise<result<int, string>>
}

module MakeWriter = (
  W: {
    type t
    let storeName: string
    let data: unit => array<t>
  },
): (Writer with type t = W.t) => {
  include W

  module Impl = ReIndexedPatterns.MakeStaticWriter({
    include W
    let data = W.data()
  })

  let write = (transaction, clear) =>
    Impl.write(transaction->IDB__Migration__Transaction.downcast, clear)
}

include ReIndexedCommands

module type Rewriter = {
  type before
  type after
  type extra
  let storeName: string
  let process: (before, extra) => command<after>
  let rewrite: (IDB__Migration__Transaction.t<'db>, extra) => promise<result<unit, string>>
}

module MakeRewriter = (
  R: {
    type before
    type after
    type extra
    let storeName: string
    let process: (before, extra) => command<after>
  },
): (Rewriter with type before = R.before and type after = R.after and type extra = R.extra) => {
  include R
  module Rewriter = ReIndexedPatterns.MakeRewriter(R)
  let rewrite = (transaction, extra) =>
    Rewriter.rewrite(transaction->IDB__Migration__Transaction.downcast, extra)
}

module type SimpleRewriter = {
  type t
  let storeName: string
  let process: t => command<t>
  let rewrite: IDB__Migration__Transaction.t<'db> => promise<result<unit, string>>
}

module MakeSimpleRewriter = (
  T: {
    type t
    let storeName: string
    let process: t => command<t>
  },
): (SimpleRewriter with type t = T.t) => {
  type t = T.t

  let storeName = T.storeName
  let process = T.process

  module Rewriter = MakeRewriter({
    type before = t
    type after = t
    type extra = unit
    let storeName = T.storeName
    let process = (record, _) => T.process(record)
  })

  let rewrite = Rewriter.rewrite(_, ())
}

module type SimpleInfallibleRewriter = {
  type t
  let storeName: string
  let process: t => t
  let rewrite: IDB__Migration__Transaction.t<'db> => promise<result<unit, string>>
}

module MakeSimpleInfallibleRewriter = (
  R: {
    type t
    let storeName: string
    let process: t => t
  },
) => {
  module Transformer = ReIndexedPatterns.MakeTransformer({
    include R
    let transform = R.process
  })
  include R
  let rewrite = t => Transformer.rewrite(t->IDB__Migration__Transaction.downcast)
}
