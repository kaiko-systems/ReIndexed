@@uncurried

type t
@val @scope("IDBKeyRange") external only: 'a => t = "only"
@val @scope("IDBKeyRange") external upperBound: ('a, bool) => t = "upperBound"
@val @scope("IDBKeyRange") external lowerBound: ('a, bool) => t = "lowerBound"
@val @scope("IDBKeyRange") external bound: ('a, 'a, bool, bool) => t = "bound"
