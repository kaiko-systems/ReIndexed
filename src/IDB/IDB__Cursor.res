@@uncurried

type t
@get external value: t => 'value = "value"
@get external key: t => 'key = "key"
@send external advance: (t, int) => unit = "advance"
@send external continue: t => unit = "continue"
@send external continueTo: (t, 'key) => unit = "continue"
@send external update: (t, 'value) => IDB__UpdateRequest.t = "update"
@send external delete: t => IDB__DeleteRequest.t = "delete"
