@doc("A command of what to do with the current value of a cursor.")
type command<'t> = Next | Update('t) | Delete
