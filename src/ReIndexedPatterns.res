open RescriptCore

@@doc("Lower level DB interations patterns.

This module contains patterns to read and transform the DB in a lower-level
fashion (but higher level than dealing with the IndexedDB directly).  You
won't need to keep cursors and almost every thing is nicely exposed in
functors and returning promises.

")
include ReIndexedCommands

type processor<'key, 'value> = Key('key => bool) | Value('value => bool)
type bound<'t> = Included('t) | Excluded('t) | Unbounded
type range<'t> = (bound<'t>, bound<'t>)

@doc("The type of the general consumer.

This is the pattern that actually implements all the other with simpler APIs.
It basically only require the types and store names, leaving other options to
as optional parameters of the function 'read'.

It can read values, only keys, follow the primary key or other index in the
store.

")
module type GeneralConsumer = {
  type t
  type index
  type key
  type value

  let storeName: string

  type request
  let make: unit => request
  let offset: (request, int) => request
  let limit: (request, int) => request
  let noLimit: request => request
  let range: (request, bound<key>, bound<key>) => request
  let noRange: request => request
  let index: (request, index) => request
  let noIndex: request => request
  let direction: (request, IDB__Index.direction) => request

  let do: (request, IDB__Transaction.t<'db>, processor<key, t>) => promise<unit>
  let read: (
    IDB__Transaction.t<'db>,
    processor<key, t>,
    ~index: index=?,
    ~offset: int=?,
    ~limit: int=?,
    ~range: range<key>=?,
    ~direction: IDB__Index.direction=?,
  ) => promise<unit>
}

module MakeGeneralConsumer = (
  R: {
    type t
    type index
    type key
    type value
    let storeName: string
  },
): (
  GeneralConsumer
    with type t = R.t
    and type index = R.index
    and type value = R.value
    and type key = R.key
) => {
  include R

  let read = (
    transaction: _,
    processor: _,
    ~index: _=?,
    ~offset: _=0,
    ~limit: _=?,
    ~range: _=?,
    ~direction: _=#next,
  ): promise<unit> => {
    let offset = if offset < 0 {
      Console.warn("Invalid negative offset in PagedConsumer; making it 0")
      0
    } else {
      offset
    }
    let limit = limit->Option.map(limit =>
      if limit < 0 {
        Console.warn("Invalid negative limit in PagedConsumer; making it 0")
        0
      } else {
        limit
      }
    )

    Promise.make((resolve, reject) => {
      reject->ignore

      let offset = ref(
        if offset > 0 {
          Some(offset)
        } else {
          None
        },
      )
      let processed = ref(0)
      switch transaction->IDB__Transaction.objectStore(storeName) {
      | store =>
        let keyRange = switch range {
        | None | Some((Unbounded, Unbounded)) => None
        | Some((Included(x), Unbounded)) => Some(IDB__KeyRange.lowerBound(x, false))
        | Some((Excluded(x), Unbounded)) => Some(IDB__KeyRange.lowerBound(x, true))
        | Some((Unbounded, Included(y))) => Some(IDB__KeyRange.upperBound(y, false))
        | Some((Unbounded, Excluded(y))) => Some(IDB__KeyRange.upperBound(y, true))
        | Some((Included(x), Included(y))) =>
          if x == y {
            Some(IDB__KeyRange.only(x))
          } else {
            Some(IDB__KeyRange.bound(x, y, false, false))
          }
        | Some((Excluded(x), Included(y))) => Some(IDB__KeyRange.bound(x, y, true, false))
        | Some((Included(x), Excluded(y))) => Some(IDB__KeyRange.bound(x, y, false, true))
        | Some((Excluded(x), Excluded(y))) => Some(IDB__KeyRange.bound(x, y, true, true))
        }
        let request = switch (index, processor) {
        | (None, Value(_)) => store->IDB__Store.openCursor(keyRange, direction)
        | (Some(index), Value(_)) =>
          store->IDB__Store.index(index)->IDB__Index.openCursor(keyRange, direction)

        | (None, Key(_)) => store->IDB__Store.openKeyCursor(keyRange, direction)
        | (Some(index), Key(_)) =>
          store->IDB__Store.index(index)->IDB__Index.openKeyCursor(keyRange, direction)
        }
        request->IDB__Request.onsuccess(event =>
          switch event->IDB__CursorEvent.cursor->Nullable.toOption {
          | None => resolve()
          | Some(cursor) =>
            switch offset.contents {
            | None => {
                processed := processed.contents + 1
                let continue = switch processor {
                | Value(process) => {
                    let item: t = cursor->IDB__Cursor.value
                    process(item)
                  }

                | Key(process) =>
                  let item: key = cursor->IDB__Cursor.key
                  process(item)
                }

                if continue && limit->Option.map(l => processed.contents < l)->Option.getOr(true) {
                  cursor->IDB__Cursor.continue
                } else {
                  resolve()
                }
              }

            | Some(many) =>
              cursor->IDB__Cursor.advance(many)
              offset := None
            }
          }
        )
      | exception _ => {
          Console.error2("Could not open store in PagedConsumer; ignoring.", storeName)
          resolve()
        }
      }
    })
  }

  type request = {
    offset: int,
    limit: option<int>,
    range: option<range<key>>,
    index: option<index>,
    direction: IDB__Index.direction,
  }

  let make = () => {
    offset: 0,
    limit: None,
    range: None,
    index: None,
    direction: #next,
  }
  let offset = (request, offset) => {...request, offset}
  let noLimit = request => {...request, limit: None}
  let limit = (request, limit) => {...request, limit: Some(limit)}
  let index = (request, index) => {...request, index: Some(index)}
  let noIndex = request => {...request, index: None}
  let range = (request, lower, upper) => {...request, range: Some((lower, upper))}
  let noRange = request => {...request, range: None}
  let direction = (request, direction) => {...request, direction}

  let do = (request, transaction, processor) =>
    read(
      transaction,
      processor,
      ~offset=request.offset,
      ~limit=?request.limit,
      ~index=request.index,
      ~range=?request.range,
      ~direction=request.direction,
    )
}

module type PagedConsumer = {
  type t
  let offset: int
  let limit: option<int>
  let storeName: string
  let process: t => bool
  let read: IDB__Transaction.t<'db> => promise<unit>
}

@doc("Create a module to read (a page) from a store.

We call `process` for each item read; and stop either when `limit` items have
been read or `process` returns false.

")
module MakePagedConsumer = (
  R: {
    type t
    let storeName: string
    let offset: int
    let limit: option<int>
    let process: t => bool
  },
): (PagedConsumer with type t = R.t) => {
  include R
  module Impl = MakeGeneralConsumer({
    type t = R.t
    type key
    type index
    type value
    let storeName = R.storeName
  })

  let read = Impl.read(_, Value(R.process), ~offset=R.offset, ~limit=?R.limit)
}

@doc("Module type for a standard consumer of all tuples of a table.")
module type Consumer = {
  type t
  let storeName: string
  let process: t => bool
  let read: IDB__Transaction.t<'db> => promise<unit>
}

@doc("Create a module to read objects of a store.")
module MakeConsumer = (
  R: {
    type t
    let storeName: string
    let process: t => bool
  },
): (Consumer with type t = R.t) => MakePagedConsumer({
  include R
  let offset = 0
  let limit = None
})

module type PagedReader = {
  type t
  let storeName: string
  let offset: int
  let limit: option<int>
  let read: IDB__Transaction.t<'db> => promise<array<t>>
}

module MakePagedReader = (
  R: {
    type t
    let storeName: string
    let offset: int
    let limit: option<int>
  },
): (PagedReader with type t = R.t) => {
  include R

  let read = transaction => {
    let result = ref([])
    module Consumer = MakePagedConsumer({
      include R
      let process = item => {
        result.contents->Array.push(item)
        true
      }
    })

    Consumer.read(transaction)->Promise.thenResolve(_ => result.contents)
  }
}

module type Reader = {
  type t
  let storeName: string
  let read: IDB__Transaction.t<'db> => promise<array<t>>
}

module MakeReader = (
  R: {
    type t
    let storeName: string
  },
): (Reader with type t = R.t) => MakePagedReader({
  include R
  let offset = 0
  let limit = None
})

@doc("The type of a simple counter")
module type Counter = {
  type t
  let storeName: string
  let predicate: t => bool
  let do: IDB__Transaction.t<'db> => promise<int>
}

@doc("Create a counter that counts items matching some predicate.")
module MakeCounter = (
  T: {
    type t
    let storeName: string
    let predicate: t => bool
  },
): (Counter with type t = T.t) => {
  include T
  let do = transaction => {
    let result = ref(0)
    let processor = Value(
      v => {
        if predicate(v) {
          result := result.contents + 1
        }
        true
      },
    )
    module Consumer = MakeGeneralConsumer({
      include T
      type value
      type key
      type index
    })

    Consumer.read(transaction, processor)->Promise.thenResolve(_ => result.contents)
  }
}

module type Writer = {
  type t
  let storeName: string
  let next: unit => option<t>
  let write: (IDB__Transaction.t<'db>, bool) => promise<result<int, string>>
}

@doc("Create a writer for the store.

The function `write` calls `next` until it returns None.  IMPORTANT: You
cannot store items with the same primary key, so you must be sure not to
produce them.  Alternatively you can request to clear the store before
starting to append items.

")
module MakeWriter = (
  W: {
    type t
    let storeName: string
    let next: unit => option<t>
  },
): (Writer with type t = W.t) => {
  include W
  let write = (transaction: _, clear: bool): promise<result<int, string>> => {
    Promise.make((resolve, reject) => {
      reject->ignore

      switch transaction->IDB__Transaction.mode {
      | #readwrite | #versionchange =>
        switch transaction->IDB__Transaction.objectStore(storeName) {
        | store =>
          let result = ref(0)
          let rec writenext = next =>
            switch next() {
            | None => resolve(Ok(result.contents))
            | Some(item) =>
              switch store->IDB__Store.add(item) {
              | request =>
                request->IDB__Request.onsuccess(_ => {
                  result := result.contents + 1
                  writenext(next)
                })
              | exception _ => {
                  Console.error2("Error while adding item", item)
                  resolve(Error("Could not add item"))
                }
              }
            }

          if clear {
            store->IDB__Store.clear->IDB__Request.onsuccess(_ => writenext(W.next))
          } else {
            writenext(W.next)
          }

        | exception _ => {
            Console.error2("Could not open the store in Writer", storeName)
            resolve(Error(`Could not open the store "${storeName}"`))
          }
        }

      | #readonly => resolve(Error("Readonly transaction"))
      }
    })
  }
}

module type StaticWriter = {
  type t
  let storeName: string
  let data: array<t>
  let next: unit => option<t>
  let write: (IDB__Transaction.t<'db>, bool) => promise<result<int, string>>
}
module MakeStaticWriter = (
  W: {
    type t
    let storeName: string
    let data: array<t>
  },
): (StaticWriter with type t = W.t) => {
  include W
  module Writer = MakeWriter({
    include W
    let index = ref(0)
    let next = () => {
      let res = data->Array.get(index.contents)
      index := index.contents + 1
      res
    }
  })

  let next = Writer.next
  let write = Writer.write
}

@doc("Module type for a standard transformer of all tuples of a table.

The types `before` and `after` represent the types of the objects in
`storeName` before and after the migration.

The function `process` should take a record of type `before` and return a
command to do with the record (nothing, update it with and `after` value, or
remove it).

The function `rewrite` will call rewrite on each record and accumulate the
results; per record, if 'rewrite' returns Ok, update the record in the DB,
otherwise *remove* the record from the DB.

The type `extra` is any type that allows the migration to get external
data and or to keep track of things.

")
module type Rewriter = {
  type before
  type after
  type extra
  let storeName: string
  let process: (before, extra) => command<after>
  let rewrite: (IDB__Transaction.t<'db>, extra) => promise<result<unit, string>>
}

module MakeRewriter = (
  R: {
    type before
    type after
    type extra
    let storeName: string
    let process: (before, extra) => command<after>
  },
): (Rewriter with type before = R.before and type after = R.after and type extra = R.extra) => {
  include R

  let rewrite = (transaction: _, extra: _): promise<result<unit, _>> => {
    Promise.make((resolve, reject) => {
      reject->ignore
      switch transaction->IDB__Transaction.mode {
      | #readonly => resolve(Error("Readonly transaction"))
      | #readwrite | #versionchange =>
        switch transaction->IDB__Transaction.objectStore(storeName) {
        | store =>
          store
          ->IDB__Store.openCursor(None, #next)
          ->IDB__Request.onsuccess(event =>
            switch event->IDB__CursorEvent.cursor->Nullable.toOption {
            | None => resolve(Ok())
            | Some(cursor) =>
              let item: before = cursor->IDB__Cursor.value
              switch R.process(item, extra) {
              | Next => cursor->IDB__Cursor.continue

              | Update(updated) =>
                cursor
                ->IDB__Cursor.update(updated)
                ->IDB__UpdateRequest.onsuccess(_ => cursor->IDB__Cursor.continue)

              | Delete =>
                cursor
                ->IDB__Cursor.delete
                ->IDB__DeleteRequest.onsuccess(_ => cursor->IDB__Cursor.continue)
              }
            }
          )

        | exception _ => {
            Console.error2("Could not open the store in Rewriter", storeName)
            resolve(Error(`Could not open the store "${storeName}"`))
          }
        }
      }
    })
  }
}

module type SimpleRewriter = {
  type t
  let storeName: string
  let process: t => command<t>
  let rewrite: IDB__Transaction.t<'db> => promise<result<unit, string>>
}

module MakeSimpleRewriter = (
  T: {
    type t
    let storeName: string
    let process: t => command<t>
  },
): SimpleRewriter => {
  include T

  module Impl = MakeRewriter({
    type before = t
    type after = t
    type extra = unit
    let storeName = T.storeName
    let process = (record, _) => T.process(record)
  })

  let rewrite = Impl.rewrite(_, ())
}

module type Transformer = {
  type t
  let storeName: string
  let transform: t => t
  let rewrite: IDB__Transaction.t<'db> => promise<result<unit, string>>
}

module MakeTransformer = (
  T: {
    type t
    let storeName: string
    let transform: t => t
  },
): (Transformer with type t = T.t) => {
  include T

  module Impl = MakeRewriter({
    type before = t
    type after = t
    type extra = unit

    let storeName = T.storeName
    let process = (record, _) => Update(T.transform(record))
  })

  let rewrite = Impl.rewrite(_, ())
}

module type Cleaner = {
  type t
  let storeName: string
  let keep: t => bool
  let clean: IDB__Transaction.t<'db> => promise<result<unit, string>>
}
module MakeCleaner = (
  T: {
    type t
    let storeName: string
    let keep: t => bool
  },
): Cleaner => {
  include T
  module Rewriter = MakeSimpleRewriter({
    include T
    let process = item =>
      if T.keep(item) {
        Next
      } else {
        Delete
      }
  })

  let clean = Rewriter.rewrite
}
