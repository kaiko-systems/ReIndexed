@@uncurried
open RescriptCore

@doc("Disconnects and drop the DB.  Notice that if there are opened connections
      to the DB (in other tabs, for instance), the operation might take a while.")
let dropdb: string => promise<result<unit, exn>> = dbname => {
  Promise.make((resolve, _) => {
    let request = IDB__Factory.deleteDatabase(dbname)

    request->IDB__OpenDBRequest.onsuccess(_ => resolve(Ok()))
    request->IDB__OpenDBRequest.onerror(_ =>
      resolve(Error(request->IDB__OpenDBRequest.error->Option.getExn))
    )
  })
}
