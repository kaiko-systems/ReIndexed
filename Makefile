PATH := ./node_modules/.bin/:$(PATH)

node_modules yarn.lock: rescript.json package.json
	yarn install
	touch node_modules

RESCRIPT_FILES := $(shell find src -type f -name '*.res')

ifdef INSIDE_EMACS
RUN := NINJA_ANSI_FORCED=0 rescript
else
RUN := rescript
endif

ifdef CI
ifndef COMPILE_STRICT
COMPILE_STRICT := 1
endif
endif


compile: $(RESCRIPT_FILES) yarn.lock node_modules
ifeq ($(COMPILE_STRICT), 1)
	$(RUN) build -with-deps -warn-error "+8+11+26+33+56+27"
else
	$(RUN) build -with-deps
endif

format: yarn.lock node_modules
	rescript format -all

FRONT_ENDPORT ?= 4369
run: compile
	vite --port $(FRONT_ENDPORT)

META_VITE_BUILD_MODE ?= production
META_VITE_BASE ?= /
build: compile
	vite build --mode $(META_VITE_BUILD_MODE) --base=$(META_VITE_BASE)

preview: build
	vite preview --port $(FRONT_ENDPORT) --mode $(META_VITE_BUILD_MODE)

ALPHA := $(shell cat package.json | jq '.version' | grep '\-alpha' >/dev/null && echo "--tag alpha" || echo "")
BETA := $(shell cat package.json | jq '.version' | grep '\-beta' >/dev/null && echo "--tag beta" || echo "")
RC := $(shell cat package.json | jq '.version' | grep '\-rc' >/dev/null && echo "--tag rc" || echo "")

publish: build
	yarn publish --access public $(ALPHA) $(BETA) $(RC)

.PHONY: format publish preview build run
