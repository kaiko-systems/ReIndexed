# ReIndexed

Kind of an IndexedDB ORM written in ReScript with no runtime dependencies.

NOTICE that this is an alpha release, which means the API is still not fixed.
We have been working on this package for over two years.  But in the last
months we have created two APIs.  The higher level ORM like API, and a lower
level which doesn't hide transactions and expose usage patterns.

## POC

The file
[tests](https://gitlab.com/kaiko-systems/ReIndexed/-/blob/main/tests/index.res)
contains an example of usage.

You want watch the tests live here: https://kaiko-systems.gitlab.io/ReIndexed/


# A note about API stability

The API of the module `ReIndexed` module is considered stable and won't change
in a backwards-breaking manner without a major version bump.

The API of the more modules `ReIndexedPatterns` and `IDB.Migration.Utils` is
not considered yet stable.  So can break it in releases without a major
version bump, but not without minor version bump.


